﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestion_Oportunidad
{
    public class Cls_DatosRequerimiento
    {

        #region Columnas Principales Ingenieria
        public String requerimiento { get; set; }
        public String servicio { get; set; }
        public String descripcion { get; set; }
        public Int32 cod_cliente { get; set; }
        #endregion

        //Constructor
        public Cls_DatosRequerimiento()
        {
            requerimiento = string.Empty;
            servicio = string.Empty;
            descripcion = string.Empty;
            cod_cliente = 0;
        }
    }
}
