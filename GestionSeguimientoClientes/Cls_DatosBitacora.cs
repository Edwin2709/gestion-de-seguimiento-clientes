﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestion_Oportunidad
{
    public class Cls_DatosBitacora
    {
        #region Columnas Principales Ingenieria
        public String tipocomunicacion { get; set; }
        public String personacontactada { get; set; }
        public String numerollamada { get; set; }
        public String observaciones { get; set; }
        public Int32 codrequerimiento { get; set; }
        public string fechaRegistro { get; set; }
        public string horaRegistro { get; set; }
        public string origen { get; set; }
        public string destino { get; set; }
        #endregion

        //Constructor
        public Cls_DatosBitacora()
        {
            tipocomunicacion = string.Empty;
            personacontactada = string.Empty;
            numerollamada = string.Empty;
            observaciones = string.Empty;
            codrequerimiento = 0;
            fechaRegistro = string.Empty;
            horaRegistro = string.Empty;
            origen = string.Empty;
            destino = string.Empty;
        }

    }
}
