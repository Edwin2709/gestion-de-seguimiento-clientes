﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sis_GestionOportunidad_DAO
{
    public sealed class DatabaseHelper
    {
        public const string conexionData = "ConexionBD";

        public static string GetConnectionString()
        {
            string settings;
            settings = ConfigurationManager.ConnectionStrings["connSqlServer"].ToString();
            return settings;
        }
        public static string GetDbConnectionString(string ConnectionString)
        {
            return ConfigurationManager.ConnectionStrings[ConnectionString].ConnectionString;
        }
    }
}
