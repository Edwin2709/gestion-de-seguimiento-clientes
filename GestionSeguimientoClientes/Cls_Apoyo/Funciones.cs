﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.IO;
using System.Data;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Microsoft.Win32;
using System.Diagnostics;
//using Auditoria;
//using Sis_Tracking_Controversias_UTIL;
//using Sis_Tracking_Controversias_BL;
namespace Controversias.Cls_Apoyo
{
    public class Funciones
    {
        OfficeOpenXml.ExcelPackage pck;
        System.IO.FileStream stream;
        //OfficeOpenXml.ExcelRangeBase firstRowCell;
     
        public static string RowToString(DataRow row)
        {
            return string.Join("|", row.ItemArray);
        }

        public DataTable ImportalExcelEPPlus(string rutaexcel, string hoja)
        {
            DataTable dt = new DataTable();
            Boolean hasHeaderRow = true;
            try
            {
                using (pck = new ExcelPackage())
                {
                    using (stream = File.OpenRead(rutaexcel))
                    {
                        pck.Load(stream);
                    }
                    ExcelWorksheet worksheet = pck.Workbook.Worksheets[1];

                    if (worksheet.Dimension == null)
                    {
                        return dt;
                    }

                    //add the columns to the datatable
                    for (int j = worksheet.Dimension.Start.Column; j <= worksheet.Dimension.End.Column; j++)
                    {
                        string columnName = "Column " + j;
                        var excelCell = worksheet.Cells[1, j].Value;

                        if (excelCell != null)
                        {
                            var excelCellDataType = excelCell;

                            //if there is a headerrow, set the next cell for the datatype and set the column name
                            if (hasHeaderRow == true)
                            {
                                excelCellDataType = worksheet.Cells[2, j].Value;

                                columnName = excelCell.ToString();

                                //check if the column name already exists in the datatable, if so make a unique name
                                if (dt.Columns.Contains(columnName) == true)
                                {
                                    columnName = columnName + "_" + j;
                                }
                            }

                            //try to determine the datatype for the column (by looking at the next column if there is a header row)
                            if (excelCellDataType is DateTime)
                            {
                                dt.Columns.Add(columnName, typeof(DateTime));
                            }
                            else if (excelCellDataType is Boolean)
                            {
                                dt.Columns.Add(columnName, typeof(Boolean));
                            }
                            else if (excelCellDataType is Double)
                            {
                                //determine if the value is a decimal or int by looking for a decimal separator
                                //not the cleanest of solutions but it works since excel always gives a double
                                if (excelCellDataType.ToString().Contains(".") || excelCellDataType.ToString().Contains(","))
                                {
                                    dt.Columns.Add(columnName, typeof(Decimal));
                                }
                                else
                                {
                                    dt.Columns.Add(columnName, typeof(Int64));
                                }
                            }
                            else
                            {
                                dt.Columns.Add(columnName, typeof(String));
                            }
                        }
                        else
                        {
                            dt.Columns.Add(columnName, typeof(String));
                        }
                    }

                    //start adding data the datatable here by looping all rows and columns
                    for (int i = worksheet.Dimension.Start.Row + Convert.ToInt32(hasHeaderRow); i <= worksheet.Dimension.End.Row; i++)
                    {
                        //create a new datatable row
                        DataRow row = dt.NewRow();

                        //loop all columns
                        for (int j = worksheet.Dimension.Start.Column; j <= worksheet.Dimension.End.Column; j++)
                        {
                            var excelCell = worksheet.Cells[i, j].Value;

                            //add cell value to the datatable
                            if (excelCell != null)
                            {
                                try
                                {
                                    row[j - 1] = excelCell;
                                }
                                catch
                                {
                                    //errorMessages += "Row " + (i - 1) + ", Column " + j + ". Invalid " + dt.Columns[j - 1].DataType.ToString().Replace("System.", "") + " value:  " + excelCell.ToString() + "<br>";
                                }
                            }
                        }

                        //add the new row to the datatable
                        dt.Rows.Add(row);
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.HResult != 2147467261)
                {
                    MessageBox.Show("Error al Importar el Archivo" + Convert.ToString(ex));
                }
            }
            return dt;
        }



        public DataTable WorksheetToDataTable(ExcelWorksheet ws, Boolean hasHeader = true)
        {
            DataTable dt = new DataTable(ws.Name);
            int totalcols = ws.Dimension.End.Column;
            int totalrows = ws.Dimension.End.Row;
            int startRow = hasHeader ? 2 : 1;
            ExcelRange wsrow;
            DataRow dr;
            int contadorRepetido = 0;

            foreach (OfficeOpenXml.ExcelRangeBase firstRowCell in ws.Cells[1, 1, 1, totalcols])
            {

                try
                {
                    dt.Columns.Add(hasHeader ? firstRowCell.Text : string.Format("Column {0}", firstRowCell.Start.Column));
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error: " + ex);
                    contadorRepetido = contadorRepetido + 1;
                    dt.Columns.Add(hasHeader ? firstRowCell.Text + Convert.ToString(contadorRepetido) : string.Format("Column {0}", firstRowCell.Start.Column));
                }
            }
            int rownum;
            for (rownum = startRow; rownum <= totalrows; rownum += 1)
            {
                ws.Cells["A5"].Style.Numberformat.Format = "dd/MM/";
                ws.Cells["A5"].Formula = "=DATE(2014,10,5)";
                wsrow = ws.Cells[rownum, 1, rownum, totalcols];
                dr = dt.NewRow();
                foreach (OfficeOpenXml.ExcelRangeBase cell in wsrow)
                {
                    dr[cell.Start.Column - 1] = cell.Text;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }


        public void ExportarExcelEPPlus(DataTable dtbase, string ruta)
        {
            try
            {
                FileInfo file = new FileInfo(ruta);
                if (file.Exists)
                {
                    file.Delete();
                }
                using (pck = new ExcelPackage(file))
                {
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Hoja1");
                    ws.Cells["A1"].LoadFromDataTable((dtbase), true);
                    ws.Cells.AutoFitColumns();

                    using (ExcelRange rng = ws.Cells[1, 1, 1, dtbase.Columns.Count])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        rng.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(79, 129, 189));
                        rng.Style.Font.Color.SetColor(System.Drawing.Color.White);
                    }

                    pck.Save();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al generar excel. Original error: " + ex.Message);
            }
        }



        //public DataTable FormatoBaseWebAmex(DataTable dtEstructura, DataTable dtBruto)
        //{

        //    for (int i = 0; i < dtBruto.Rows.Count - 1; i++)
        //    {
        //        string Var = dtBruto.Rows[i][0].ToString();
        //        string importe = "";
        //        if (Var != "")
        //        {
        //            importe = dtBruto.Rows[i][22].ToString().Substring(0, (dtBruto.Rows[i][22].ToString().Length - 2)) + "." + dtBruto.Rows[i][22].ToString().Substring((dtBruto.Rows[i][22].ToString().Length - 2), 2);
        //            DataRow dr = dtEstructura.NewRow();
        //            dr[0] = dtBruto.Rows[i][0];
        //            dr[1] = dtBruto.Rows[i][20];
        //            dr[2] = dtBruto.Rows[i][21];
        //            dr[3] = Convert.ToDecimal(importe);
        //            if (dtBruto.Rows[i][23].ToString().Substring(0, 1).Equals("6")) { dr[4] = "SOLES"; } // moneda
        //            else if (dtBruto.Rows[i][23].ToString().Substring(0, 1).Equals("8")) { dr[4] = "DOLARES"; }
        //            dr[5] = Convert.ToDateTime(dtBruto.Rows[i][25].ToString().Substring(0, 4) + "/" + dtBruto.Rows[i][25].ToString().Substring(4, 2) + "/" + dtBruto.Rows[i][25].ToString().Substring(6, 2));
        //            dr[6] = dtBruto.Rows[i][37];
        //            dtEstructura.Rows.Add(dr);
        //            dr = null;
        //        }

        //    }

        //    return dtEstructura;
        //}

        //public DataTable Estado_Amex(DataTable dtWebAmex, Sis_Tracking_Controversias_BE.Cls_WebAmex objWebAmex, Cls_Bases_Ingreso_Logica logBase_Ingreso)
        //{
        //    //Sis_Tracking_Controversias_BE.Cls_WebAmex objWebAmex = new Sis_Tracking_Controversias_BE.Cls_WebAmex();
        //    //Sis_Tracking_Controversias_BL.Cls_Bases_Ingreso_Logica logBase_Ingreso = new Sis_Tracking_Controversias_BL.Cls_Bases_Ingreso_Logica();
        //    DataTable dtEstadoAmex = new DataTable();
        //    int resultado;

        //    //limpiamos la tabla
        //    logBase_Ingreso.Refresar_Tabla_WebAmex();
        //    // Cargamos la tabla Web Amex

        //    for (int i = 0; i <= dtWebAmex.Rows.Count - 1; i += 1)
        //    {
        //        objWebAmex.NroRegistro = dtWebAmex.Rows[i]["NroRegistro"].ToString();
        //        objWebAmex.Num_Cta_Principal = dtWebAmex.Rows[i]["Num_Cta_principal"].ToString();
        //        objWebAmex.TID = dtWebAmex.Rows[i]["TID"].ToString();
        //        objWebAmex.Monto_Fraud = Convert.ToDecimal(dtWebAmex.Rows[i]["Monto_fraud"]);
        //        objWebAmex.Moneda = dtWebAmex.Rows[i]["Cod_Moneda_Fraud"].ToString();
        //        objWebAmex.Fecha_fraud = Convert.ToDateTime(dtWebAmex.Rows[i]["Fecha_fraud"]);
        //        objWebAmex.ARN = dtWebAmex.Rows[i]["ARN"].ToString();
        //        resultado = logBase_Ingreso.Subir_WebAmex(objWebAmex);
        //    }

        //    // Hacemos la consulta
        //    dtEstadoAmex = logBase_Ingreso.Estado_Cruce_Amex();
        //    return dtEstadoAmex;
        //}



        //public DataTable ConvertToDataTable_TC(string filePath)
        //{

        //    DataTable dt = new DataTable("Coldview");
        //    dt.Columns.Add("Cuenta", typeof(string));
        //    dt.Columns.Add("ARN", typeof(string));
        //    dt.Columns.Add("Moneda", typeof(string));
        //    dt.Columns.Add("Importe", typeof(double));
        //    dt.Columns.Add("Tipo_Cuenta", typeof(string));
        //    dt.Columns.Add("Fecha_ContraCargo", typeof(string));
        //    dt.Columns.Add("BP", typeof(string));
        //    DataRow workRow;


        //    using (StreamReader sr = new StreamReader(filePath))
        //    {
        //        string linea;
        //        string fecha = string.Empty;
        //        bool encontro_fecha = false;
        //        while ((linea = sr.ReadLine()) != null)
        //        {
        //            //string s1 = linea;
        //            //string s2 = "MONEDA";
        //            //bool b = s1.Contains(s2);
        //            //string moneda = string.Empty;
        //            //if (b==true)
        //            //    {
        //            //        moneda = Regex.Replace(linea, "MONEDA:", "").Trim();
        //            //    }
        //            string[] campos = linea.Trim().Split(' '); //Corta por los tabs
        //            long result;
        //            string arn = string.Empty;

        //            if (linea.Contains("TARJETA DE DEBITO"))
        //            {
        //                Dictionary<string, string> parametros = new Dictionary<string, string>();//AUDITORIA
        //                parametros.Add("<AccountController>", "Error al escoger el tipo de archivo a guardar: " + Globales.gsMatriculaUsuario);
        //                Auditoria.AdministradorLog.escribirLogAuditoria("0", AdministradorLog.ObtenerNombrePorIP(Sis_Tracking_Controversias_UTIL.Globales.gsIPEquipo), parametros);
        //                MessageBox.Show("Error al seleccionar el tipo de archivo a subir");

        //                MessageBox.Show("Error al subir el tipo de formato");
        //                dt = null;
        //                return dt;
        //            }

        //            if (linea.Contains("B.C.P"))
        //            {
        //                for (int i = 0; i <= campos.Length - 1; i += 1)
        //                {
        //                    DateTime Temp;
        //                    if (DateTime.TryParse(campos[i].ToString(), out Temp) == true)
        //                    {
        //                        fecha = campos[i].ToString();
        //                        break;
        //                    }

        //                }
        //            }

        //            if (linea.Contains("252401000001") & linea.Contains("BP"))
        //            {

        //                for (int i = 0; i <= campos.Length; i += 1)
        //                {
        //                    if (campos[i].Length == 23)
        //                    {
        //                        if (long.TryParse(campos[i].Substring(1, 12), out result) == true)
        //                        {
        //                            arn = campos[i].ToString();
        //                            workRow = dt.NewRow();
        //                            workRow[0] = "252401000001";
        //                            workRow[1] = arn;
        //                            workRow[2] = "DOLARES";
        //                            workRow[3] = double.Parse(campos[campos.Length - 1]);
        //                            workRow[4] = "TC";
        //                            workRow[5] = fecha;
        //                            workRow[6] = campos[i + 1].ToString();
        //                            dt.Rows.Add(workRow);
        //                            break;
        //                        }
        //                    }
        //                }
        //            }
        //            else if (linea.Contains("251401000001") & linea.Contains("BP"))
        //            {
        //                for (int i = 0; i <= campos.Length - 1; i += 1)
        //                {

        //                    if (campos[i].Length == 23)
        //                    {
        //                        if (long.TryParse(campos[i].Substring(1, 12), out result) == true)
        //                        {
        //                            arn = campos[i].ToString();
        //                            workRow = dt.NewRow();
        //                            workRow[0] = "251401000001";
        //                            workRow[1] = arn;
        //                            workRow[2] = "SOLES";
        //                            workRow[3] = double.Parse(campos[campos.Length - 1]);
        //                            workRow[4] = "TC";
        //                            workRow[5] = fecha;
        //                            workRow[6] = campos[i + 1].ToString();
        //                            dt.Rows.Add(workRow);
        //                            break;

        //                        }
        //                    }
        //                }
        //            }
        //        }

        //    }
        //    return dt;
        //}


        //public DataTable ConvertToDataTable_TD(string filePath)
        //{
        //    DataTable dt = new DataTable("Coldview");
        //    dt.Columns.Add("Cuenta", typeof(string));
        //    dt.Columns.Add("ARN", typeof(string));
        //    dt.Columns.Add("Moneda", typeof(string));
        //    dt.Columns.Add("Importe", typeof(double));
        //    dt.Columns.Add("Tipo_Cuenta", typeof(string));
        //    dt.Columns.Add("Fecha_ContraCargo", typeof(string));
        //    dt.Columns.Add("BP", typeof(string));
        //    DataRow workRow;

        //    using (StreamReader sr = new StreamReader(filePath))
        //    {
        //        string linea;
        //        bool encontro_fecha = false;
        //        string fecha = string.Empty;
        //        while ((linea = sr.ReadLine()) != null)
        //        {
        //            string[] campos = linea.Trim().Split(' '); //Corta por los tabs
        //            long result;
        //            string arn = string.Empty;

        //            if (linea.Contains("TARJETA DE CREDITO"))
        //            {
        //                Dictionary<string, string> parametros = new Dictionary<string, string>();//AUDITORIA
        //                parametros.Add("<AccountController>", "Error al escoger el tipo de archivo a guardar: " + Globales.gsMatriculaUsuario);
        //                Auditoria.AdministradorLog.escribirLogAuditoria("0", AdministradorLog.ObtenerNombrePorIP(Sis_Tracking_Controversias_UTIL.Globales.gsIPEquipo), parametros);
        //                MessageBox.Show("Error al seleccionar el tipo de archivo a subir");
        //                MessageBox.Show("Error al subir el tipo de formato");
        //                dt = null;
        //                return dt;
        //            }

        //            if (linea.Contains("B.C.P") & encontro_fecha == false)
        //            {
        //                for (int i = 0; i <= campos.Length - 1; i += 1)
        //                {
        //                    DateTime Temp;
        //                    if (DateTime.TryParse(campos[i].ToString(), out Temp) == true)
        //                    {
        //                        fecha = campos[i].ToString();
        //                        break;
        //                    }
        //                }
        //            }

        //            if (linea.Contains("74547758213082130185399"))
        //            {
        //                MessageBox.Show("");
        //            }


        //            if (linea.Contains("252401000002") & linea.Contains("BP"))
        //            {
        //                for (int i = 0; i <= campos.Length; i += 1)
        //                {
        //                    if (campos[i].Length == 23)
        //                    {
        //                        if (long.TryParse(campos[i].Substring(1, 12), out result) == true)
        //                        {
        //                            arn = campos[i].ToString();
        //                            workRow = dt.NewRow();
        //                            workRow[0] = "252401000002";
        //                            workRow[1] = arn;
        //                            workRow[2] = "DOLARES";
        //                            workRow[3] = double.Parse(campos[campos.Length - 1]);
        //                            workRow[4] = "TD";
        //                            workRow[5] = fecha;
        //                            workRow[6] = campos[i + 1].ToString();
        //                            dt.Rows.Add(workRow);
        //                            break;
        //                        }
        //                    }
        //                }
        //            }

        //            else if (linea.Contains("251401000002") & linea.Contains("BP"))
        //            {
        //                for (int i = 0; i <= campos.Length - 1; i += 1)
        //                {
        //                    if (campos[i].Length == 23)
        //                    {
        //                        if (long.TryParse(campos[i].Substring(1, 12), out result) == true)
        //                        {
        //                            arn = campos[i].ToString();
        //                            workRow = dt.NewRow();
        //                            workRow[0] = "251401000002";
        //                            workRow[1] = arn;
        //                            workRow[2] = "SOLES";
        //                            workRow[3] = double.Parse(campos[campos.Length - 1]);
        //                            workRow[4] = "TD";
        //                            workRow[5] = fecha;
        //                            workRow[6] = campos[i + 1].ToString();
        //                            dt.Rows.Add(workRow);
        //                            break;
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }

        //    return dt;
        //}


        //#region "AMEX"
        //public DataSet ConvertToDataTable_TC_AMEX(string filePath)
        //{

        //    DataSet DS = new DataSet("ColdViewAmex");
        //    DataTable dtContracargo = new DataTable("Contracargo");
        //    DataTable dtFraude = new DataTable("Fraude");
        //    DS.Tables.Add(dtContracargo);
        //    DS.Tables.Add(dtFraude);

        //    //******************************* cargo ********************
        //    dtContracargo.Columns.Add("Cuenta", typeof(string));
        //    dtContracargo.Columns.Add("Tit", typeof(string));
        //    dtContracargo.Columns.Add("Tipo_Cuenta", typeof(string));
        //    dtContracargo.Columns.Add("Moneda", typeof(string));
        //    dtContracargo.Columns.Add("Importe", typeof(double));
        //    dtContracargo.Columns.Add("Fecha_ContraCargo", typeof(string));
        //    //dtContracargo.Columns.Add("BP", typeof(string));
        //    DataRow workRowCargo;
        //    workRowCargo = dtContracargo.NewRow();
        //    //**********************  fraude ************************************
        //    dtFraude.Columns.Add("Cuenta", typeof(string));
        //    dtFraude.Columns.Add("Tit", typeof(string));
        //    dtFraude.Columns.Add("Tipo_Cuenta", typeof(string));
        //    dtFraude.Columns.Add("Moneda", typeof(string));
        //    dtFraude.Columns.Add("Importe", typeof(double));
        //    dtFraude.Columns.Add("Fecha_ContraCargo", typeof(string));
        //    //dtFraude.Columns.Add("BP", typeof(string));
        //    DataRow workRowFraude;
        //    workRowFraude = dtFraude.NewRow();



        //    using (StreamReader sr = new StreamReader(filePath))
        //    {
        //        string linea;
        //        string Marca = "";
        //        string fecha = string.Empty;
        //        string[] datos = new string[7];
        //        string Cuenta = string.Empty;
        //        string importe = "";
        //        int contador = 0;
        //        int pbAvance;
        //        double dblPorcentaje;
        //        double pbTotal = 200;


        //        while ((linea = sr.ReadLine()) != null)
        //        {

        //            //pgProgreso.Value= contador;
        //            pbAvance = (contador + 1);
        //            dblPorcentaje = Math.Round((pbAvance / pbTotal) * 100, 2);
        //            // BackgroundWorker.ReportProgress(dblPorcentaje);

        //            // limpiamos el array 
        //            string[] campos = linea.Trim().Split(' '); //Corta por los tabs
        //            long result;
        //            string arn = string.Empty;
        //            if (linea.Contains("TARJETA DE DEBITO"))
        //            {
        //                Dictionary<string, string> parametros = new Dictionary<string, string>();//AUDITORIA
        //                parametros.Add("<AccountController>", "Error al escoger el tipo de archivo a guardar: " + Globales.gsMatriculaUsuario);
        //                Auditoria.AdministradorLog.escribirLogAuditoria("0", AdministradorLog.ObtenerNombrePorIP(Sis_Tracking_Controversias_UTIL.Globales.gsIPEquipo), parametros);
        //                MessageBox.Show("Error al seleccionar el tipo de archivo a subir");
        //                MessageBox.Show("Error al subir el tipo de formato");
        //                dtContracargo = null;
        //                return DS;
        //            }

        //            if (linea.Contains("CURR PROCNG DATE"))
        //            {
        //                for (int i = 0; i <= campos.Length - 1; i += 1)
        //                {
        //                    DateTime Temp;
        //                    if (DateTime.TryParse(campos[i].ToString(), out Temp) == true)
        //                    {
        //                        fecha = campos[i].ToString();
        //                        break;
        //                    }

        //                }
        //            }

        //            if (linea.Contains("OUTGOING"))
        //            {
        //                Marca = "contracargo";
        //            }
        //            else if (linea.Contains("FRAUD"))
        //            {
        //                Marca = "fraude";

        //            }

        //            if (Marca.Equals("contracargo"))
        //            {

        //                if (campos[0] != "")
        //                {
        //                    // tarjeta 
        //                    //******************** validacion para saber el numero de cuenta ***********

        //                    //string vars = linea.ToString().Substring(3, 15).Trim();

        //                    if (linea.Length >= 15 && linea.ToString() != "")
        //                    {

        //                        if (IsNumeric(linea.ToString().Substring(4, 15).Trim()) == true)
        //                        {

        //                            Cuenta = linea.ToString().Substring(4, 15).Trim();


        //                            if (Cuenta.Substring(0, 2) != "00" && Cuenta.Length >= 15)
        //                            {
        //                                datos[0] = Cuenta; // nro cuenta
        //                                importe = linea.ToString().Substring(32, 7).Trim(); // monto
        //                                importe = importe.Substring(0, (importe.Length - 2)) + "." + importe.Substring((importe.Length - 2), 2);
        //                                datos[1] = importe; // monto
        //                                datos[6] = linea.ToString().Substring(32, 7).Trim(); // monto referencia

        //                            }
        //                        }

        //                        string cadena = linea.ToString().Substring(35, 7).Trim();
        //                        string cadena2 = linea.ToString().Substring(29, 7).Trim();

        //                        if (datos[6] == cadena || datos[6] == cadena2) //comparar montos
        //                        {

        //                            //************************************** validacion para saber el TIT
        //                            //== true && IsNumeric(linea.ToString().Substring(19, 4).Trim())
        //                            if (IsNumeric(linea.ToString().Substring(4, 18).Trim()))
        //                            {

        //                                for (int i = 0; i <= campos.Length - 1; i += 1)
        //                                {

        //                                    if (campos[i].Length == 15)
        //                                    {
        //                                        if (long.TryParse(campos[i].Substring(0, 15), out result) == true)
        //                                        {
        //                                            arn = campos[i].ToString();
        //                                            datos[2] = arn;
        //                                        }
        //                                    }

        //                                    if (campos[i].Equals(datos[6]))
        //                                    {
        //                                        datos[3] = campos[i];
        //                                    }

        //                                    if (campos[i].Length == 3 && (campos[i].Substring(0, 1).Equals("8") || campos[i].Substring(0, 1).Equals("6") || campos[i].Substring(0, 1).Equals("9")))
        //                                    {
        //                                        datos[4] = campos[i];
        //                                        break;
        //                                    }
        //                                }
        //                            }

        //                            //*********************************************************************************************
        //                        }
        //                        //string s = linea.ToString().Substring(4, 15).Trim();
        //                        if (IsNumeric(linea.ToString().Substring(15, 4).Trim()) && linea.ToString().Substring(4, 15).Trim().Equals(datos[6]))
        //                        {
        //                            for (int i = 0; i <= campos.Length - 1; i += 1)
        //                            {
        //                                if (campos[i].Equals(datos[4]))
        //                                {

        //                                    workRowCargo = dtContracargo.NewRow();
        //                                    workRowCargo[0] = datos[0]; // nro cuenta
        //                                    workRowCargo[1] = datos[2];  // tit
        //                                    workRowCargo[2] = "CONTRACARGO"; //Tipo de accion
        //                                    if (campos[i].Substring(0, 1).Equals("6")) { workRowCargo[3] = "SOLES"; } // moneda
        //                                    else if (campos[i].Substring(0, 1).Equals("8")) { workRowCargo[3] = "DOLARES"; }
        //                                    else if (campos[i].Substring(0, 1).Equals("9")) { workRowCargo[3] = "NO DEFINIDO"; }
        //                                    workRowCargo[4] = Convert.ToDouble(datos[1]);
        //                                    workRowCargo[5] = linea.ToString().Substring(29, 10).Trim();
        //                                    dtContracargo.Rows.Add(workRowCargo);

        //                                    break;
        //                                }
        //                            }

        //                            Array.Clear(datos, 0, 6);
        //                        }





        //                    }


        //                    //if (IsNumeric(linea.ToString().Substring(4, 15).Trim()) == true)
        //                    //{
        //                    //    Cuenta = linea.ToString().Substring(4, 15).Trim();


        //                    //    if (Cuenta.Substring(0, 2) != "00" && Cuenta.Length >=15)
        //                    //    {
        //                    //        datos[0] = Cuenta; // nro cuenta
        //                    //        importe = linea.ToString().Substring(32, 7).Trim(); // monto
        //                    //        importe = importe.Substring(0, (importe.Length - 2)) + "." + importe.Substring((importe.Length - 2), 2);
        //                    //        datos[1] = importe; // monto
        //                    //        datos[6] = linea.ToString().Substring(32, 7).Trim(); // monto referencia

        //                    //    }

        //                    //}
        //                    //******************************************************************** 



        //                }

        //            }
        //            else if (Marca.Equals("fraude"))
        //            {
        //                if (campos[0] != "")
        //                {

        //                    if (linea.Length > 15)
        //                    {
        //                        // tarjeta 
        //                        //******************** validacion para saber el numero de cuenta ***********
        //                        if (IsNumeric(linea.ToString().Substring(4, 15).Trim()))
        //                        {
        //                            Cuenta = linea.ToString().Substring(4, 15).Trim();

        //                            if (Cuenta.Substring(0, 2) != "00" && Cuenta.Length >= 15)
        //                            {
        //                                datos[0] = Cuenta;

        //                                importe = linea.ToString().Substring(33, 7).Trim();
        //                                importe = importe.Substring(0, (importe.Length - 2)) + "." + importe.Substring((importe.Length - 2), 2);
        //                                datos[1] = importe;

        //                                datos[6] = linea.ToString().Substring(33, 7).Trim(); // monto de referencia
        //                            }
        //                        }
        //                        //******************************************************************** 

        //                        //string cadena = linea.ToString().Substring(35, 7).Trim();
        //                        string cadena2 = linea.ToString().Substring(57, 7).Trim();

        //                        //if (datos[1] == cadena || datos[1] == cadena2)
        //                        if (datos[6] == cadena2)
        //                        {

        //                            //************************************** validacion para saber el TIT
        //                            //== true && IsNumeric(linea.ToString().Substring(19, 4).Trim())
        //                            if (IsNumeric(linea.ToString().Substring(4, 16).Trim()))
        //                            {

        //                                for (int i = 0; i <= campos.Length - 1; i += 1)
        //                                {
        //                                    if (campos[i].Length == 15)
        //                                    {
        //                                        if (long.TryParse(campos[i].Substring(0, 15), out result) == true)
        //                                        {
        //                                            arn = campos[i].ToString();
        //                                            datos[2] = arn;
        //                                            datos[5] = linea.ToString().Substring(27, 11).Trim();
        //                                        }
        //                                    }

        //                                    if (campos[i].Equals(datos[6])) // importe
        //                                    {
        //                                        datos[3] = campos[i];
        //                                    }

        //                                    if (campos[i].Length == 3 && (campos[i].Substring(0, 1).Equals("8") || campos[i].Substring(0, 1).Equals("6"))) //guardar tipo moneda
        //                                    {
        //                                        datos[4] = campos[i];
        //                                        break;
        //                                    }


        //                                }
        //                                //Array.Clear(datos, 0, 3);
        //                            }

        //                            //*********************************************************************************************
        //                        }


        //                        if (IsNumeric(linea.ToString().Substring(15, 4).Trim()) && linea.ToString().Substring(4, 15).Trim().Equals(datos[6]))
        //                        {
        //                            for (int i = 0; i <= campos.Length - 1; i += 1)
        //                            {
        //                                if (campos[i].Equals(datos[4]))
        //                                {

        //                                    workRowCargo = dtContracargo.NewRow();
        //                                    workRowCargo[0] = datos[0]; // nro cuenta
        //                                    workRowCargo[1] = datos[2];  // tit
        //                                    workRowCargo[2] = "FRAUDE"; //Tipo de accion
        //                                    if (campos[i].Substring(0, 1).Equals("6")) { workRowCargo[3] = "SOLES"; } // moneda
        //                                    else if (campos[i].Substring(0, 1).Equals("8")) { workRowCargo[3] = "DOLARES"; }
        //                                    workRowCargo[4] = Convert.ToDouble(datos[1]);
        //                                    workRowCargo[5] = datos[5];
        //                                    dtContracargo.Rows.Add(workRowCargo);

        //                                    break;
        //                                }
        //                            }

        //                            Array.Clear(datos, 0, 6);
        //                        }

        //                    }

        //                }

        //            }
        //            contador = contador + 1;
        //        }


        //    }
        //    return DS;
        //}
        //public static Boolean IsNumeric(string valor)
        //{
        //    bool resultado = true;
        //    valor = valor.Trim();
        //    try
        //    {
        //        Int64 num = Convert.ToInt64(valor);

        //    }
        //    catch (Exception)
        //    {
        //        resultado = false;
        //        //throw;
        //    }
        //    return resultado;
        //}
        //public void ValidarRB(RadioButton[] ArrayRB, RadioButton RBInicial)
        //{
        //    if (RBInicial.Checked == true)
        //    {
        //        for (int i = 0; i <= ArrayRB.Length - 1; i++)
        //        {
        //            if (ArrayRB[i].Equals(RBInicial))
        //            {
        //                ArrayRB[i].Checked = true;
        //            }
        //            else
        //            {
        //                ArrayRB[i].Checked = false;
        //            }

        //        }
        //    }

        //}

        //public DataTable EstructuraWebAmex()
        //{
        //    DataTable dt = new DataTable();
        //    DataColumn pkOrderID =
        //    dt.Columns.Add("NroRegistro", typeof(string));
        //    dt.Columns.Add("Num_Cta_principal", typeof(string));
        //    dt.Columns.Add("TID", typeof(string));
        //    dt.Columns.Add("Monto_fraud", typeof(Decimal));
        //    dt.Columns.Add("Cod_Moneda_Fraud", typeof(string));
        //    dt.Columns.Add("Fecha_fraud", typeof(DateTime));
        //    dt.Columns.Add("ARN", typeof(string));
        //    return dt;
        //}
        //#endregion

        public string GuardarArchivo()
        {
            System.Windows.Forms.OpenFileDialog ofdSeleccionarArchivo = new System.Windows.Forms.OpenFileDialog();
            ofdSeleccionarArchivo.Filter = "All Files (*.*)|*.*";
            ofdSeleccionarArchivo.FilterIndex = 1;
            ofdSeleccionarArchivo.RestoreDirectory = true;

            string archivoOrigen= string.Empty;
            string rutaDestino=string.Empty;
            string archivoDestino= string.Empty;

            if (ofdSeleccionarArchivo.ShowDialog() == DialogResult.OK)
            {
                try
                {
                     archivoOrigen = ofdSeleccionarArchivo.FileName;
                     rutaDestino = @"D:\Edwin\Proyectos GIT\1";
                     archivoDestino = System.IO.Path.Combine(rutaDestino, ofdSeleccionarArchivo.SafeFileName);

                    if (!System.IO.Directory.Exists(rutaDestino))
                    {
                        System.IO.Directory.CreateDirectory(rutaDestino);
                    }

                    System.IO.File.Copy(archivoOrigen, archivoDestino, true);
                    MessageBox.Show("Archivo copiado exitosamente");

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

            return archivoDestino;
        }

        public string Guardar_Multiples_Archivos() {
            var o = new System.Windows.Forms.OpenFileDialog();
            o.Multiselect = true;

            string archivoOrigen = string.Empty;
            string rutaDestino = string.Empty;
            string archivoDestino = string.Empty;

            if (o.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    var failedToUploads = new List<string>();
                    var uploads = new List<string>();

                    o.FileNames.ToList().ForEach(file =>
                    {
                        archivoOrigen = Path.GetDirectoryName(file);
                        rutaDestino = @"D:\Edwin\Proyectos GIT\1";
                        archivoDestino = System.IO.Path.Combine(rutaDestino, Path.GetFileName(file));
                        int cont = 0;

                        if (!System.IO.File.Exists(archivoDestino))
                        {
                            System.IO.File.Copy(archivoOrigen, archivoDestino, true);
                            cont++;
                            uploads.Add(file);
                        }
                        else
                        {
                            failedToUploads.Add(file);
                        }
                        var message = string.Format("Archivos Subidos Correctamente: \n {0}", string.Join("\n", uploads.ToArray()));
                        if (failedToUploads.Count > 0)
                            message += string.Format("\nArchivos NO Subidos: \n {0}", string.Join("\n", failedToUploads.ToArray()));

                        MessageBox.Show(message);

                    });
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
                

            }

            return "";
        }

        public void Guardar_archivo_ruta(string rutaOrigen)
        {
            string rutaDestino = string.Empty;
            string archivoDestino = string.Empty;
            int cont;
            string archivoOrigen = rutaOrigen.Substring(rutaOrigen.LastIndexOf("\\") + 1);
            MessageBox.Show("Seleccione la ruta para guardar el archivo:", "Guardar Archivo");
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            
            DialogResult resultado = dialog.ShowDialog();
            if (resultado == DialogResult.OK)
            {
                try
                {
                    var failedToUploads = new List<string>();
                    var uploads = new List<string>();
                    cont = 0;
                    rutaDestino = dialog.SelectedPath;
                    archivoDestino = System.IO.Path.Combine(rutaDestino, archivoOrigen);
                    if (!System.IO.File.Exists(archivoOrigen))
                    {
                        System.IO.File.Copy(rutaOrigen, archivoDestino, true);
                        cont++;
                        uploads.Add(archivoOrigen);
                    }
                    else
                    {
                        failedToUploads.Add(archivoOrigen);
                    }
                    var message = string.Format("Archivo Descargado Correctamente: {0} \n ¿Desea abrir el archivo?", string.Join("\n", uploads.ToArray()));
                    if (failedToUploads.Count > 0)
                        message += string.Format("\nArchivos NO Subidos: \n {0}", string.Join("\n", failedToUploads.ToArray()));

                    DialogResult messagePrompt = MessageBox.Show(message,"Finalizado",MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);
                    if (messagePrompt == DialogResult.Yes)
                    {
                        Process.Start(archivoDestino);
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK ,MessageBoxIcon.Exclamation);
                }
            }
        }

        //public static Boolean SendEmailWithOutlook(string mailDirection, string mailSubject, string mailContent)
        //{
        //    try
        //    {
        //        var oApp = new Microsoft.Office.Interop.Outlook.Application();

        //        Microsoft.Office.Interop.Outlook.NameSpace ns = oApp.GetNamespace("MAPI");
        //        var f = ns.GetDefaultFolder(Microsoft.Office.Interop.Outlook.OlDefaultFolders.olFolderInbox);

        //        System.Threading.Thread.Sleep(1000);

        //        var mailItem = (Microsoft.Office.Interop.Outlook.MailItem)oApp.CreateItem(Microsoft.Office.Interop.Outlook.OlItemType.olMailItem);
        //        mailItem.Subject = mailSubject;
        //        mailItem.HTMLBody = mailContent;
        //        mailItem.To = mailDirection;
        //        mailItem.Send();

        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //    finally
        //    {
        //    }
        //    return true;
        //}

    //    //  Application.EnableVisualStyles();
    //    Application.SetCompatibleTextRenderingDefault(false);
 
    //string mailDirection = @"direccionTest@test.com";
    //    string mailSubject = "Subject test";

    //    string mainContent;
    //    mainContent = "Mensaje de prueba";
    //mainContent += "<br>" + "Otra línea";
    //mainContent += "<br>" + "Otra línea";
 
    //SendEmailWithOutlook(mailDirection, mailSubject, mailContent);
    }

 
}



