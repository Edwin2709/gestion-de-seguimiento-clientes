﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GestionSeguimientoClientes;
using GestionSeguimientoClientes.Comercial;

namespace Gestion_Oportunidad.Comercial
{
    public partial class Menu_Comercial : Form
    {
        Cls_Registro_Dao objcom = new Cls_Registro_Dao();
        string equipo = string.Empty;

        public Menu_Comercial(string equipo_usuario)
        {
            equipo = equipo_usuario;
            InitializeComponent();
        }

        private void Menu_Comercial_Load(object sender, EventArgs e)
        {

            dgvpendiente.DataSource = objcom.Consultar_Clientes("comercial", Environment.UserName);
            this.txtempresa.Text = "";
            txtempresa.AutoCompleteCustomSource = objcom.GetData();
            txtempresa.AutoCompleteMode = AutoCompleteMode.Suggest;
            txtempresa.AutoCompleteSource = AutoCompleteSource.CustomSource;

        }

        private void Menu_Comercial_DoubleClick(object sender, EventArgs e)
        {
        }



        private void dgvpendiente_DoubleClick(object sender, EventArgs e)
        {
            //ENCONTRAR EL ID DEL DATAGRIEW Y CONSULTAR EN LA BD
            string id = Convert.ToString(this.dgvpendiente.CurrentRow.Cells[0].Value);
            Comercial_ Hijo = new Comercial_(id, equipo);
            Hijo.StartPosition = FormStartPosition.CenterScreen;
            this.Hide();
            if (Hijo.ShowDialog(this) == DialogResult.OK)
            {

            }
            this.Show();

            //------------------ CONSULTAR BD  PARA DATAGRIEVVIEW PENDIENTE DEPENDIENDO EL EQUIPO ------------------------------------
            switch (equipo)
            {
                case "comercial":
                    //ESTADOS DE COMERCIAL
                    dgvpendiente.DataSource = objcom.Consultar_Clientes("comercial", Environment.UserName);
                    break;
            }
        }

        private void reportesToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void txtempresa_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyData == Keys.Enter)
            {
                if (txtempresa.Text != "")
                {
                    dgvpendiente.DataSource = objcom.Consultar_Clientes_Razosocial(txtempresa.Text);
                }
                else
                {
                    dgvpendiente.DataSource = objcom.Consultar_Clientes("comercial", Environment.UserName);
                }
            }
        }

        private void txtempresa_TextChanged(object sender, EventArgs e)
        {
            if (txtempresa.Text == "")
            {
                dgvpendiente.DataSource = objcom.Consultar_Clientes("comercial", Environment.UserName);
            }
        }
    }
}
