﻿using Gestion_Oportunidad;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionSeguimientoClientes.Comercial
{
    public partial class Comercial_ : Form
    {
        Cls_Registro_Dao objcom = new Cls_Registro_Dao();
        Cls_DatosRequerimiento objdatos = new Cls_DatosRequerimiento();
        Cls_DatosBitacora objbitac = new Cls_DatosBitacora();
        int cod = 0;
        int cod_requerimiento = 0;
        string equipo_usuario = string.Empty;
        public Comercial_(string id, string equipo)
        {
            cod = Convert.ToInt32(id);
            equipo_usuario = equipo;
            InitializeComponent();
        }

        public Comercial_()
        {
        }

        private void Comercial_Load(object sender, EventArgs e)
        {
            Llenar_Datos();
            Llenar_Autocompletado();
            //groupBox1.Enabled = false;

        }

        public void Llenar_Autocompletado()
        {
            this.txtpersonacontactada.Text = "";
            txtpersonacontactada.AutoCompleteCustomSource = objcom.GetContactoData();
            txtpersonacontactada.AutoCompleteMode = AutoCompleteMode.Suggest;
            txtpersonacontactada.AutoCompleteSource = AutoCompleteSource.CustomSource;

            this.txtnumerollamada.Text = "";
            txtnumerollamada.AutoCompleteCustomSource = objcom.GetLlamadaData();
            txtnumerollamada.AutoCompleteMode = AutoCompleteMode.Suggest;
            txtnumerollamada.AutoCompleteSource = AutoCompleteSource.CustomSource;
        }

        public void Llenar_Datos()
        {
            DataTable dtClientes = objcom.Consultar_Clientes_cod(cod);
            txtrazonsocial.Text = dtClientes.Rows[0]["Razon_Social"].ToString();
            txtdireccion.Text = dtClientes.Rows[0]["Direccion"].ToString();
            txtdistrito.Text = dtClientes.Rows[0]["Distrito"].ToString();
            txtprovincia.Text = dtClientes.Rows[0]["Provincia"].ToString();
            txtdeprtameento.Text = dtClientes.Rows[0]["Departamento"].ToString();
            txtmacroregion.Text = dtClientes.Rows[0]["Macroregion"].ToString();
            txtrepresentantelegal.Text = dtClientes.Rows[0]["Representante_Legal"].ToString();
            txtventas.Text = dtClientes.Rows[0]["Ventas"].ToString();
            txtaptc.Text = dtClientes.Rows[0]["APTC"].ToString();

            DataTable dtRequerimiento = objcom.Consultar_Requerimiento_x_CodCliente(cod);

            if (dtRequerimiento.Rows.Count != 0)
            {

                for (int i = 0; i <= dtRequerimiento.Rows.Count - 1; i += 1)

                    if (dtRequerimiento.Rows[i]["Requerimiento"].ToString() == "Conectividad")
                    {
                        cboservicioconecti.Text = dtRequerimiento.Rows[i]["Servicio"].ToString();
                        txtdescripcionconecti.Text = dtRequerimiento.Rows[i]["Descripcion"].ToString();
                        txtcodconectividad.Text = dtRequerimiento.Rows[i]["Id1"].ToString();
                    }
                    else if (dtRequerimiento.Rows[i]["Requerimiento"].ToString() == "Infraestructura")
                    {
                        cboservicioinfra.Text = dtRequerimiento.Rows[i]["Servicio"].ToString();
                        txtdescripcioninfra.Text = dtRequerimiento.Rows[i]["Descripcion"].ToString();
                        txtcodinfraestructura.Text = dtRequerimiento.Rows[i]["Id1"].ToString();
                    }
                    else if (dtRequerimiento.Rows[i]["Requerimiento"].ToString() == "Internet")
                    {
                        cboserviciointernet.Text = dtRequerimiento.Rows[i]["Servicio"].ToString();
                        txtdescripcioninternet.Text = dtRequerimiento.Rows[i]["Descripcion"].ToString();
                        txtcodinternet.Text = dtRequerimiento.Rows[i]["Id1"].ToString();
                    }
            }
        }


        private void btnagregar_Click_1(object sender, EventArgs e)
        {
            int codRequerimiento = 0;
         
            if (rbconect.Checked == true)
            {
                if (cboservicioconecti.Text == "")
                {
                    MessageBox.Show("Debe llenar el tipo de servicio", "Nota Importante", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                    return;
                }

                if (txtcodconectividad.Text != "")
                {
                    codRequerimiento = Convert.ToInt32(txtcodconectividad.Text);
                }

            }
            else if (rbinfra.Checked == true)
            {
                if (cboservicioinfra.Text == "")
                {
                    MessageBox.Show("Debe llenar el tipo de servicio", "Nota Importante", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                    return;
                }

                if (txtcodinfraestructura.Text != "")
                {
                    codRequerimiento = Convert.ToInt32(txtcodinfraestructura.Text);
                }
            
            }
            else if (rbinter.Checked == true)
            {
                if (cboserviciointernet.Text == "")
                {
                    MessageBox.Show("Debe llenar el tipo de servicio", "Nota Importante", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                    return;
                } 

                if (txtcodinternet.Text != "")
                {
                    codRequerimiento = Convert.ToInt32(txtcodinternet.Text);
                }              
            }
            else
            {
                MessageBox.Show("Al menos debe seleccionar un tipo de registro", "Nota Importante", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                return;
            }


            if (txtobservaciones.Text.Trim() == "")
            {
                MessageBox.Show("Al menos debe llenar el campo observacion", "Nota Importante",MessageBoxButtons.OK,MessageBoxIcon.Exclamation,MessageBoxDefaultButton.Button1);
                return;
            }


            objdatos.cod_cliente = cod;

            if (codRequerimiento != 0)
            {
                cod_requerimiento = codRequerimiento;

                objbitac.tipocomunicacion = cbotipocomunicacion.Text;
                objbitac.personacontactada = txtpersonacontactada.Text;
                objbitac.numerollamada = txtnumerollamada.Text;
                objbitac.observaciones = txtobservaciones.Text;
                objbitac.codrequerimiento = cod_requerimiento;
                objbitac.fechaRegistro = DateTime.Now.ToString("dd/MM/yyyy");
                objbitac.horaRegistro = DateTime.Now.ToShortTimeString();
                objbitac.origen = txtOrigen.Text;
                objbitac.destino = txtDestino.Text;
                objcom.Insertar_DatosBitacora(objbitac);

                dgvbitacora.DataSource = objcom.Consultar_Bitacora_x_CodRequerimiento(cod_requerimiento);
                dgvbitacora.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                MessageBox.Show("Se subió con éxito la información.", "Nota Importante", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                return;
            }

            else
            {
                if (rbconect.Checked == true)
                {
                    objdatos.requerimiento = "Conectividad";
                    objdatos.servicio = cboservicioconecti.Text;
                    objdatos.descripcion = txtdescripcionconecti.Text;
                    
                }
                else if (rbinfra.Checked == true)
                {
                    objdatos.requerimiento = "Infraestructura";
                    objdatos.servicio = cboservicioinfra.Text;
                    objdatos.descripcion = txtdescripcioninfra.Text;
                   
                }
                else if (rbinter.Checked == true)
                {
                    objdatos.requerimiento = "Internet";
                    objdatos.servicio = cboserviciointernet.Text;
                    objdatos.descripcion = txtdescripcioninternet.Text;

                  
                }
               
                cod_requerimiento = objcom.Insertar_DatosRequerimiento(objdatos);
                objbitac.tipocomunicacion = cbotipocomunicacion.Text;
                objbitac.fechaRegistro = DateTime.Now.ToString("dd/MM/yyyy");
                objbitac.horaRegistro = DateTime.Now.ToShortTimeString();
                if (rbconect.Checked == true)
                {
                    txtcodconectividad.Text = cod_requerimiento.ToString();

                }
                else if (rbinfra.Checked == true)
                {
                    txtcodinfraestructura.Text = cod_requerimiento.ToString();
                }
                else if (rbinter.Checked == true)
                {
                    txtcodinternet.Text = cod_requerimiento.ToString();
                }


                objbitac.personacontactada = txtpersonacontactada.Text;
                objbitac.numerollamada = txtnumerollamada.Text;
                objbitac.observaciones = txtobservaciones.Text;
                objbitac.codrequerimiento = cod_requerimiento;
                objbitac.origen = txtOrigen.Text;
                objbitac.destino = txtDestino.Text;
                objcom.Insertar_DatosBitacora(objbitac);
                 

                dgvbitacora.DataSource = objcom.Consultar_Bitacora_x_CodRequerimiento(cod_requerimiento);
                dgvbitacora.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

                MessageBox.Show("Se subió con éxito la información.");

            }
        }

        private void rbinfra_CheckedChanged(object sender, EventArgs e)
        {
            if (rbinfra.Checked == true)
            {
                if (txtcodinfraestructura.Text != "")
                {
                    Cls_Registro_Dao objcom = new Cls_Registro_Dao();
                    dgvbitacora.DataSource = objcom.Consultar_Bitacora_x_CodRequerimiento(Convert.ToInt32(txtcodinfraestructura.Text));
                    dgvbitacora.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                }
                else
                {
                    dgvbitacora.DataSource = null;
                }

                txtDestino.Visible = false;
                label7.Visible = false;
                txtOrigen.Visible = true;
                label1.Visible = true;
            }
           
        }

        private void rbconect_CheckedChanged(object sender, EventArgs e)
        {
            if (rbconect.Checked == true)
            {
                if (txtcodconectividad.Text != "")
                {
                    Cls_Registro_Dao objcom = new Cls_Registro_Dao();
                    dgvbitacora.DataSource = objcom.Consultar_Bitacora_x_CodRequerimiento(Convert.ToInt32(txtcodconectividad.Text));
                    dgvbitacora.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                }

                else
                {
                    dgvbitacora.DataSource = null;
                }

                txtDestino.Visible = true;
                label7.Visible = true;
                txtOrigen.Visible = true;
                label1.Visible = true;

            }
        }

        private void rbinter_CheckedChanged(object sender, EventArgs e)
        {
            if (rbinter.Checked == true)
            {
                if (txtcodinternet.Text != "")
                {
                    Cls_Registro_Dao objcom = new Cls_Registro_Dao();
                    dgvbitacora.DataSource = objcom.Consultar_Bitacora_x_CodRequerimiento(Convert.ToInt32(txtcodinternet.Text));
                    dgvbitacora.AutoSizeColumnsMode =DataGridViewAutoSizeColumnsMode.AllCells;
                }
                else
                {
                    dgvbitacora.DataSource = null;
                }

                txtDestino.Visible = false;
                label7.Visible = false;
                txtOrigen.Visible = true;
                label1.Visible = true;
            }
        }

        private void dgvbitacora_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void txtdescripcioninfra_TextChanged(object sender, EventArgs e)
        {
        }

        private void txtdescripcionconecti_TextChanged(object sender, EventArgs e)
        {
        }

        private void txtdescripcioninternet_TextChanged(object sender, EventArgs e)
        {
        }

        private void grbUbicacion_Enter(object sender, EventArgs e)
        {

        }

        private void txtpersonacontactada_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                txtnumerollamada.Text = objcom.Consultar_Numero_Contacto(txtpersonacontactada.Text); 
            }
        }
    }
}
