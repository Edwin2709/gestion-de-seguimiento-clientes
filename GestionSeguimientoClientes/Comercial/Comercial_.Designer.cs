﻿namespace GestionSeguimientoClientes.Comercial
{
    partial class Comercial_
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Comercial_));
            this.grbUbicacion = new System.Windows.Forms.GroupBox();
            this.txtdescripcioninternet = new System.Windows.Forms.ComboBox();
            this.txtdescripcionconecti = new System.Windows.Forms.ComboBox();
            this.txtdescripcioninfra = new System.Windows.Forms.ComboBox();
            this.txtcodinternet = new System.Windows.Forms.TextBox();
            this.txtcodinfraestructura = new System.Windows.Forms.TextBox();
            this.txtcodconectividad = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDestino = new System.Windows.Forms.TextBox();
            this.txtOrigen = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnagregar = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dgvbitacora = new System.Windows.Forms.DataGridView();
            this.txtobservaciones = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtnumerollamada = new System.Windows.Forms.TextBox();
            this.cbotipocomunicacion = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtpersonacontactada = new System.Windows.Forms.TextBox();
            this.rbinter = new System.Windows.Forms.RadioButton();
            this.rbconect = new System.Windows.Forms.RadioButton();
            this.rbinfra = new System.Windows.Forms.RadioButton();
            this.cboserviciointernet = new System.Windows.Forms.ComboBox();
            this.cboservicioconecti = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cboservicioinfra = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.grbPedido = new System.Windows.Forms.GroupBox();
            this.txtaptc = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtdeprtameento = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtventas = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtprovincia = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtrepresentantelegal = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtdistrito = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtrazonsocial = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtmacroregion = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtdireccion = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.grbUbicacion.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvbitacora)).BeginInit();
            this.grbPedido.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbUbicacion
            // 
            this.grbUbicacion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbUbicacion.Controls.Add(this.txtdescripcioninternet);
            this.grbUbicacion.Controls.Add(this.txtdescripcionconecti);
            this.grbUbicacion.Controls.Add(this.txtdescripcioninfra);
            this.grbUbicacion.Controls.Add(this.txtcodinternet);
            this.grbUbicacion.Controls.Add(this.txtcodinfraestructura);
            this.grbUbicacion.Controls.Add(this.txtcodconectividad);
            this.grbUbicacion.Controls.Add(this.groupBox2);
            this.grbUbicacion.Controls.Add(this.rbinter);
            this.grbUbicacion.Controls.Add(this.rbconect);
            this.grbUbicacion.Controls.Add(this.rbinfra);
            this.grbUbicacion.Controls.Add(this.cboserviciointernet);
            this.grbUbicacion.Controls.Add(this.cboservicioconecti);
            this.grbUbicacion.Controls.Add(this.label15);
            this.grbUbicacion.Controls.Add(this.label4);
            this.grbUbicacion.Controls.Add(this.label5);
            this.grbUbicacion.Controls.Add(this.label26);
            this.grbUbicacion.Controls.Add(this.label3);
            this.grbUbicacion.Controls.Add(this.cboservicioinfra);
            this.grbUbicacion.Controls.Add(this.label2);
            this.grbUbicacion.Font = new System.Drawing.Font("Telefonica", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbUbicacion.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grbUbicacion.Location = new System.Drawing.Point(12, 153);
            this.grbUbicacion.Name = "grbUbicacion";
            this.grbUbicacion.Size = new System.Drawing.Size(984, 564);
            this.grbUbicacion.TabIndex = 21;
            this.grbUbicacion.TabStop = false;
            this.grbUbicacion.Text = "Datos a llenar";
            this.grbUbicacion.Enter += new System.EventHandler(this.grbUbicacion_Enter);
            // 
            // txtdescripcioninternet
            // 
            this.txtdescripcioninternet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtdescripcioninternet.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdescripcioninternet.FormattingEnabled = true;
            this.txtdescripcioninternet.Items.AddRange(new object[] {
            "En Negociación",
            "En Coordinación",
            "En Ejecución",
            "Post Venta",
            "Otros"});
            this.txtdescripcioninternet.Location = new System.Drawing.Point(642, 81);
            this.txtdescripcioninternet.Name = "txtdescripcioninternet";
            this.txtdescripcioninternet.Size = new System.Drawing.Size(243, 24);
            this.txtdescripcioninternet.TabIndex = 51;
            // 
            // txtdescripcionconecti
            // 
            this.txtdescripcionconecti.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtdescripcionconecti.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdescripcionconecti.FormattingEnabled = true;
            this.txtdescripcionconecti.Items.AddRange(new object[] {
            "En Negociación",
            "En Coordinación",
            "En Ejecución",
            "Post Venta",
            "Otros"});
            this.txtdescripcionconecti.Location = new System.Drawing.Point(642, 55);
            this.txtdescripcionconecti.Name = "txtdescripcionconecti";
            this.txtdescripcionconecti.Size = new System.Drawing.Size(243, 24);
            this.txtdescripcionconecti.TabIndex = 50;
            // 
            // txtdescripcioninfra
            // 
            this.txtdescripcioninfra.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtdescripcioninfra.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdescripcioninfra.FormattingEnabled = true;
            this.txtdescripcioninfra.Items.AddRange(new object[] {
            "En Negociación",
            "En Coordinación",
            "En Ejecución",
            "Post Venta",
            "Otros"});
            this.txtdescripcioninfra.Location = new System.Drawing.Point(642, 30);
            this.txtdescripcioninfra.Name = "txtdescripcioninfra";
            this.txtdescripcioninfra.Size = new System.Drawing.Size(243, 24);
            this.txtdescripcioninfra.TabIndex = 49;
            // 
            // txtcodinternet
            // 
            this.txtcodinternet.Location = new System.Drawing.Point(166, 84);
            this.txtcodinternet.Name = "txtcodinternet";
            this.txtcodinternet.Size = new System.Drawing.Size(35, 26);
            this.txtcodinternet.TabIndex = 48;
            this.txtcodinternet.Visible = false;
            // 
            // txtcodinfraestructura
            // 
            this.txtcodinfraestructura.Location = new System.Drawing.Point(166, 31);
            this.txtcodinfraestructura.Name = "txtcodinfraestructura";
            this.txtcodinfraestructura.Size = new System.Drawing.Size(35, 26);
            this.txtcodinfraestructura.TabIndex = 47;
            this.txtcodinfraestructura.Visible = false;
            // 
            // txtcodconectividad
            // 
            this.txtcodconectividad.Location = new System.Drawing.Point(166, 57);
            this.txtcodconectividad.Name = "txtcodconectividad";
            this.txtcodconectividad.Size = new System.Drawing.Size(35, 26);
            this.txtcodconectividad.TabIndex = 46;
            this.txtcodconectividad.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.txtDestino);
            this.groupBox2.Controls.Add(this.txtOrigen);
            this.groupBox2.Controls.Add(this.groupBox1);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.txtnumerollamada);
            this.groupBox2.Controls.Add(this.cbotipocomunicacion);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtpersonacontactada);
            this.groupBox2.Location = new System.Drawing.Point(26, 122);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(940, 436);
            this.groupBox2.TabIndex = 41;
            this.groupBox2.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(634, 86);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 16);
            this.label7.TabIndex = 55;
            this.label7.Text = "Destino:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(640, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 16);
            this.label1.TabIndex = 54;
            this.label1.Text = "Origen:";
            // 
            // txtDestino
            // 
            this.txtDestino.Location = new System.Drawing.Point(694, 81);
            this.txtDestino.Name = "txtDestino";
            this.txtDestino.Size = new System.Drawing.Size(175, 26);
            this.txtDestino.TabIndex = 53;
            // 
            // txtOrigen
            // 
            this.txtOrigen.Location = new System.Drawing.Point(694, 40);
            this.txtOrigen.Name = "txtOrigen";
            this.txtOrigen.Size = new System.Drawing.Size(175, 26);
            this.txtOrigen.TabIndex = 52;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnagregar);
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.txtobservaciones);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.groupBox1.Location = new System.Drawing.Point(5, 127);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(929, 303);
            this.groupBox1.TabIndex = 51;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Bitacora de Seguimiento";
            // 
            // btnagregar
            // 
            this.btnagregar.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnagregar.Location = new System.Drawing.Point(20, 269);
            this.btnagregar.Name = "btnagregar";
            this.btnagregar.Size = new System.Drawing.Size(87, 28);
            this.btnagregar.TabIndex = 21;
            this.btnagregar.Text = "Agregar";
            this.btnagregar.UseVisualStyleBackColor = true;
            this.btnagregar.Click += new System.EventHandler(this.btnagregar_Click_1);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgvbitacora);
            this.panel2.Location = new System.Drawing.Point(371, 39);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(538, 224);
            this.panel2.TabIndex = 20;
            // 
            // dgvbitacora
            // 
            this.dgvbitacora.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvbitacora.Location = new System.Drawing.Point(3, 3);
            this.dgvbitacora.Name = "dgvbitacora";
            this.dgvbitacora.Size = new System.Drawing.Size(532, 218);
            this.dgvbitacora.TabIndex = 0;
            // 
            // txtobservaciones
            // 
            this.txtobservaciones.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtobservaciones.Location = new System.Drawing.Point(20, 39);
            this.txtobservaciones.Multiline = true;
            this.txtobservaciones.Name = "txtobservaciones";
            this.txtobservaciones.Size = new System.Drawing.Size(345, 224);
            this.txtobservaciones.TabIndex = 19;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(17, 20);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(93, 16);
            this.label23.TabIndex = 18;
            this.label23.Text = "Observaciones:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(403, 32);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(137, 32);
            this.label20.TabIndex = 45;
            this.label20.Text = "Numero de Contacto o \r\nCorreo Contacto:";
            // 
            // txtnumerollamada
            // 
            this.txtnumerollamada.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnumerollamada.Location = new System.Drawing.Point(406, 67);
            this.txtnumerollamada.Name = "txtnumerollamada";
            this.txtnumerollamada.Size = new System.Drawing.Size(178, 24);
            this.txtnumerollamada.TabIndex = 46;
            // 
            // cbotipocomunicacion
            // 
            this.cbotipocomunicacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbotipocomunicacion.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbotipocomunicacion.FormattingEnabled = true;
            this.cbotipocomunicacion.Items.AddRange(new object[] {
            "Email",
            "Llamada",
            "Reunion Presencial",
            "Whatsapp"});
            this.cbotipocomunicacion.Location = new System.Drawing.Point(20, 67);
            this.cbotipocomunicacion.Name = "cbotipocomunicacion";
            this.cbotipocomunicacion.Size = new System.Drawing.Size(171, 24);
            this.cbotipocomunicacion.TabIndex = 44;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(17, 40);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(133, 16);
            this.label19.TabIndex = 43;
            this.label19.Text = "Tipo de Comunicacion:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(201, 40);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(124, 16);
            this.label6.TabIndex = 41;
            this.label6.Text = "Persona Contactada:";
            // 
            // txtpersonacontactada
            // 
            this.txtpersonacontactada.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpersonacontactada.Location = new System.Drawing.Point(204, 67);
            this.txtpersonacontactada.Name = "txtpersonacontactada";
            this.txtpersonacontactada.Size = new System.Drawing.Size(178, 24);
            this.txtpersonacontactada.TabIndex = 42;
            this.txtpersonacontactada.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtpersonacontactada_KeyDown);
            // 
            // rbinter
            // 
            this.rbinter.AutoSize = true;
            this.rbinter.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbinter.Location = new System.Drawing.Point(53, 87);
            this.rbinter.Name = "rbinter";
            this.rbinter.Size = new System.Drawing.Size(71, 20);
            this.rbinter.TabIndex = 45;
            this.rbinter.TabStop = true;
            this.rbinter.Text = "Internet";
            this.rbinter.UseVisualStyleBackColor = true;
            this.rbinter.CheckedChanged += new System.EventHandler(this.rbinter_CheckedChanged);
            // 
            // rbconect
            // 
            this.rbconect.AutoSize = true;
            this.rbconect.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbconect.Location = new System.Drawing.Point(53, 60);
            this.rbconect.Name = "rbconect";
            this.rbconect.Size = new System.Drawing.Size(98, 20);
            this.rbconect.TabIndex = 44;
            this.rbconect.TabStop = true;
            this.rbconect.Text = "Conectividad";
            this.rbconect.UseVisualStyleBackColor = true;
            this.rbconect.CheckedChanged += new System.EventHandler(this.rbconect_CheckedChanged);
            // 
            // rbinfra
            // 
            this.rbinfra.AutoSize = true;
            this.rbinfra.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbinfra.Location = new System.Drawing.Point(51, 34);
            this.rbinfra.Name = "rbinfra";
            this.rbinfra.Size = new System.Drawing.Size(109, 20);
            this.rbinfra.TabIndex = 43;
            this.rbinfra.TabStop = true;
            this.rbinfra.Text = "Infraestructura";
            this.rbinfra.UseVisualStyleBackColor = true;
            this.rbinfra.CheckedChanged += new System.EventHandler(this.rbinfra_CheckedChanged);
            // 
            // cboserviciointernet
            // 
            this.cboserviciointernet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboserviciointernet.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboserviciointernet.FormattingEnabled = true;
            this.cboserviciointernet.Items.AddRange(new object[] {
            "Internet@s"});
            this.cboserviciointernet.Location = new System.Drawing.Point(335, 83);
            this.cboserviciointernet.Name = "cboserviciointernet";
            this.cboserviciointernet.Size = new System.Drawing.Size(164, 24);
            this.cboserviciointernet.TabIndex = 42;
            // 
            // cboservicioconecti
            // 
            this.cboservicioconecti.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboservicioconecti.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboservicioconecti.FormattingEnabled = true;
            this.cboservicioconecti.Items.AddRange(new object[] {
            "Arrendado",
            "Lan to Lan",
            "IPVPN"});
            this.cboservicioconecti.Location = new System.Drawing.Point(335, 57);
            this.cboservicioconecti.Name = "cboservicioconecti";
            this.cboservicioconecti.Size = new System.Drawing.Size(164, 24);
            this.cboservicioconecti.TabIndex = 41;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(554, 85);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(75, 16);
            this.label15.TabIndex = 38;
            this.label15.Text = "Descripcion:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(554, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 16);
            this.label4.TabIndex = 37;
            this.label4.Text = "Descripcion:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(278, 89);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 16);
            this.label5.TabIndex = 36;
            this.label5.Text = "Servicio";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(278, 65);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(51, 16);
            this.label26.TabIndex = 35;
            this.label26.Text = "Servicio";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(554, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Descripcion:";
            // 
            // cboservicioinfra
            // 
            this.cboservicioinfra.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboservicioinfra.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboservicioinfra.FormattingEnabled = true;
            this.cboservicioinfra.Items.AddRange(new object[] {
            "Acceso FO",
            "Energia",
            "Incx",
            "Outdoor",
            "Postes",
            "Rack"});
            this.cboservicioinfra.Location = new System.Drawing.Point(335, 32);
            this.cboservicioinfra.Name = "cboservicioinfra";
            this.cboservicioinfra.Size = new System.Drawing.Size(164, 24);
            this.cboservicioinfra.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(278, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Servicio";
            // 
            // grbPedido
            // 
            this.grbPedido.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbPedido.Controls.Add(this.txtaptc);
            this.grbPedido.Controls.Add(this.label10);
            this.grbPedido.Controls.Add(this.txtdeprtameento);
            this.grbPedido.Controls.Add(this.label17);
            this.grbPedido.Controls.Add(this.txtventas);
            this.grbPedido.Controls.Add(this.label24);
            this.grbPedido.Controls.Add(this.txtprovincia);
            this.grbPedido.Controls.Add(this.label25);
            this.grbPedido.Controls.Add(this.txtrepresentantelegal);
            this.grbPedido.Controls.Add(this.label14);
            this.grbPedido.Controls.Add(this.txtdistrito);
            this.grbPedido.Controls.Add(this.label9);
            this.grbPedido.Controls.Add(this.txtrazonsocial);
            this.grbPedido.Controls.Add(this.label11);
            this.grbPedido.Controls.Add(this.txtmacroregion);
            this.grbPedido.Controls.Add(this.label12);
            this.grbPedido.Controls.Add(this.txtdireccion);
            this.grbPedido.Controls.Add(this.label8);
            this.grbPedido.Font = new System.Drawing.Font("Telefonica", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbPedido.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grbPedido.Location = new System.Drawing.Point(12, 12);
            this.grbPedido.Name = "grbPedido";
            this.grbPedido.Size = new System.Drawing.Size(984, 135);
            this.grbPedido.TabIndex = 20;
            this.grbPedido.TabStop = false;
            this.grbPedido.Text = "Datos del Cliente";
            // 
            // txtaptc
            // 
            this.txtaptc.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtaptc.Location = new System.Drawing.Point(642, 91);
            this.txtaptc.Name = "txtaptc";
            this.txtaptc.ReadOnly = true;
            this.txtaptc.Size = new System.Drawing.Size(121, 24);
            this.txtaptc.TabIndex = 25;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(639, 72);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(75, 16);
            this.label10.TabIndex = 24;
            this.label10.Text = "APTC (si/no)";
            // 
            // txtdeprtameento
            // 
            this.txtdeprtameento.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdeprtameento.Location = new System.Drawing.Point(806, 42);
            this.txtdeprtameento.Name = "txtdeprtameento";
            this.txtdeprtameento.ReadOnly = true;
            this.txtdeprtameento.Size = new System.Drawing.Size(134, 24);
            this.txtdeprtameento.TabIndex = 23;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(803, 23);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(93, 16);
            this.label17.TabIndex = 22;
            this.label17.Text = "Departamento:";
            // 
            // txtventas
            // 
            this.txtventas.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtventas.Location = new System.Drawing.Point(466, 91);
            this.txtventas.Name = "txtventas";
            this.txtventas.ReadOnly = true;
            this.txtventas.Size = new System.Drawing.Size(134, 24);
            this.txtventas.TabIndex = 21;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(463, 72);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(50, 16);
            this.label24.TabIndex = 20;
            this.label24.Text = "Ventas:";
            // 
            // txtprovincia
            // 
            this.txtprovincia.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtprovincia.Location = new System.Drawing.Point(642, 41);
            this.txtprovincia.Name = "txtprovincia";
            this.txtprovincia.ReadOnly = true;
            this.txtprovincia.Size = new System.Drawing.Size(121, 24);
            this.txtprovincia.TabIndex = 19;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(639, 22);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(61, 16);
            this.label25.TabIndex = 18;
            this.label25.Text = "Provincia:";
            // 
            // txtrepresentantelegal
            // 
            this.txtrepresentantelegal.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtrepresentantelegal.Location = new System.Drawing.Point(221, 91);
            this.txtrepresentantelegal.Name = "txtrepresentantelegal";
            this.txtrepresentantelegal.ReadOnly = true;
            this.txtrepresentantelegal.Size = new System.Drawing.Size(200, 24);
            this.txtrepresentantelegal.TabIndex = 17;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(218, 72);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(126, 16);
            this.label14.TabIndex = 16;
            this.label14.Text = "Representante Legal";
            // 
            // txtdistrito
            // 
            this.txtdistrito.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdistrito.Location = new System.Drawing.Point(466, 41);
            this.txtdistrito.Name = "txtdistrito";
            this.txtdistrito.ReadOnly = true;
            this.txtdistrito.Size = new System.Drawing.Size(134, 24);
            this.txtdistrito.TabIndex = 15;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(463, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 16);
            this.label9.TabIndex = 14;
            this.label9.Text = "Distrito:";
            // 
            // txtrazonsocial
            // 
            this.txtrazonsocial.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtrazonsocial.Location = new System.Drawing.Point(9, 41);
            this.txtrazonsocial.Name = "txtrazonsocial";
            this.txtrazonsocial.ReadOnly = true;
            this.txtrazonsocial.Size = new System.Drawing.Size(165, 24);
            this.txtrazonsocial.TabIndex = 11;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(6, 23);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(82, 16);
            this.label11.TabIndex = 10;
            this.label11.Text = "Razon Social:";
            // 
            // txtmacroregion
            // 
            this.txtmacroregion.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmacroregion.Location = new System.Drawing.Point(9, 91);
            this.txtmacroregion.Name = "txtmacroregion";
            this.txtmacroregion.ReadOnly = true;
            this.txtmacroregion.Size = new System.Drawing.Size(165, 24);
            this.txtmacroregion.TabIndex = 9;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(6, 72);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(84, 16);
            this.label12.TabIndex = 8;
            this.label12.Text = "MacroRegion:";
            // 
            // txtdireccion
            // 
            this.txtdireccion.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdireccion.Location = new System.Drawing.Point(221, 41);
            this.txtdireccion.Name = "txtdireccion";
            this.txtdireccion.ReadOnly = true;
            this.txtdireccion.Size = new System.Drawing.Size(200, 24);
            this.txtdireccion.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(218, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 16);
            this.label8.TabIndex = 6;
            this.label8.Text = "Direccion:";
            // 
            // Comercial_
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.grbUbicacion);
            this.Controls.Add(this.grbPedido);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Comercial_";
            this.Text = "Comercial";
            this.Load += new System.EventHandler(this.Comercial_Load);
            this.grbUbicacion.ResumeLayout(false);
            this.grbUbicacion.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvbitacora)).EndInit();
            this.grbPedido.ResumeLayout(false);
            this.grbPedido.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grbUbicacion;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnagregar;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtobservaciones;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtnumerollamada;
        private System.Windows.Forms.ComboBox cbotipocomunicacion;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtpersonacontactada;
        private System.Windows.Forms.RadioButton rbinter;
        private System.Windows.Forms.RadioButton rbconect;
        private System.Windows.Forms.RadioButton rbinfra;
        private System.Windows.Forms.ComboBox cboserviciointernet;
        private System.Windows.Forms.ComboBox cboservicioconecti;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cboservicioinfra;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox grbPedido;
        private System.Windows.Forms.TextBox txtaptc;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtdeprtameento;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtventas;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtprovincia;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtrepresentantelegal;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtdistrito;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtrazonsocial;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtmacroregion;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtdireccion;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtcodinternet;
        private System.Windows.Forms.TextBox txtcodinfraestructura;
        private System.Windows.Forms.TextBox txtcodconectividad;
        private System.Windows.Forms.DataGridView dgvbitacora;
        private System.Windows.Forms.ComboBox txtdescripcioninternet;
        private System.Windows.Forms.ComboBox txtdescripcionconecti;
        private System.Windows.Forms.ComboBox txtdescripcioninfra;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDestino;
        private System.Windows.Forms.TextBox txtOrigen;
    }
}