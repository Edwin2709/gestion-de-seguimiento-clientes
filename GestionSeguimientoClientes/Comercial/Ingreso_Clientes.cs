﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace GUI_V_2
{
    public partial class Ingreso_Ventanilla : Form
    {
        public int xClick = 0, yClick = 0;
        public int resultado = 0;

        OpenFileDialog ofdArcExc = new OpenFileDialog();
        DataTable dtBase = new DataTable();

        Controversias.Cls_Apoyo.Funciones ImportarExcel = new Controversias.Cls_Apoyo.Funciones();

        public Ingreso_Ventanilla()
        {
            InitializeComponent();
           // this.WindowState = FormWindowState.Maximized;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Productos_Load(object sender, EventArgs e)
        {

        }

        private void BtnImportar_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofdSeleccionarArchivo = new OpenFileDialog();

            try
            {

                ofdArcExc.Filter = "Excel files (*.xls or *.xlsx or *.xlsm)|*.xls;*.xlsx;*.xlsm";
                ofdSeleccionarArchivo.FilterIndex = 1;
                ofdSeleccionarArchivo.RestoreDirectory = true;

                if (ofdSeleccionarArchivo.ShowDialog() == DialogResult.OK)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    dtBase = ImportarExcel.ImportalExcelEPPlus(ofdSeleccionarArchivo.FileName, "Hoja1");

                    if (dtBase.Rows.Count > 0)

                    {
                        //Dictionary<string, string> parametros = new Dictionary<string, string>();//AUDITORIA
                        //parametros.Add("<AccountController>", "Se importo la informacion con exito: " + LblRutaArchivo.Text + Globales.gsMatriculaUsuario);
                        //Auditoria.AdministradorLog.escribirLogAuditoria("0", AdministradorLog.ObtenerNombrePorIP(Sis_Tracking_Controversias_UTIL.Globales.gsIPEquipo), parametros);
                        //LblRutaArchivo.Text = ofdSeleccionarArchivo.FileName;

                        DataGridViewCellStyle columnHeaderStyle = new DataGridViewCellStyle();
                        columnHeaderStyle.BackColor = Color.Beige;
                        columnHeaderStyle.Font = new Font("Verdana", 8, FontStyle.Bold);
                        dgvBase.ColumnHeadersDefaultCellStyle = columnHeaderStyle;
                        dgvBase.AutoResizeColumns();
                        dgvBase.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                        dgvBase.DataSource = dtBase;
                    }
                    else
                    {
                        //Dictionary<string, string> parametros = new Dictionary<string, string>();//AUDITORIA
                        //parametros.Add("<AccountController>", "Error al importar el archivo: " + LblRutaArchivo.Text + Globales.gsMatriculaUsuario);
                        //Auditoria.AdministradorLog.escribirLogAuditoria("0", AdministradorLog.ObtenerNombrePorIP(Sis_Tracking_Controversias_UTIL.Globales.gsIPEquipo), parametros);
                    }
                }
            }
            catch (Exception a)
            {
                //Dictionary<string, string> parametros = new Dictionary<string, string>();//AUDITORIA
                //parametros.Add("<HomeController>", "Validación al presionar el botón Importar, el usuario canceló la selección de subir el archivo");
                //Auditoria.AdministradorLog.escribirLogAplicacion("0", AdministradorLog.ObtenerNombrePorIP(Sis_Tracking_Controversias_UTIL.Globales.gsIPEquipo), a, parametros);
            }
        }

        private void btnsubir_Click(object sender, EventArgs e)
        {
            try
            {
                //Dictionary<string, string> parametros = new Dictionary<string, string>();//AUDITORIA
                //parametros.Add("<AccountController>", "Accion Subir Base " + Globales.gsMatriculaUsuario);
                //Auditoria.AdministradorLog.escribirLogAuditoria("0", AdministradorLog.ObtenerNombrePorIP(Sis_Tracking_Controversias_UTIL.Globales.gsIPEquipo), parametros);

                //SUBIR LA DATA A LA BASE DE VENTANILLA
                subir_data(dtBase);
            }
            catch (Exception a)
            {
                //Dictionary<string, string> parametros = new Dictionary<string, string>();//AUDITORIA
                //parametros.Add("<HomeController>", "Validación al presionar el botón Importar, el usuario canceló la selección de subir el archivo");
                //Auditoria.AdministradorLog.escribirLogAplicacion("0", AdministradorLog.ObtenerNombrePorIP(Sis_Tracking_Controversias_UTIL.Globales.gsIPEquipo), a, parametros);
            }

        }


        private void Ingreso_Ventanilla_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
            { xClick = e.X; yClick = e.Y; }
            else
            { this.Left = this.Left + (e.X - xClick); this.Top = this.Top + (e.Y - yClick); }
        }

        public void subir_data(DataTable dtventanilla)
        {
            string nombre_cabecera = string.Empty;
            //int x = 0;

            foreach (DataColumn column in dtBase.Columns)
            {
                nombre_cabecera = column.ColumnName;
                break;
            }

            if (nombre_cabecera == "VUM")
            {

                for (int i = 0; i <= dtBase.Rows.Count - 1; i += 1)

                {
                    //Sis_GestionOportunidad_BE.Cls_Ventanilla objventanilla = new Sis_GestionOportunidad_BE.Cls_Ventanilla();
                    //Estado_Vum_ objestad = new Estado_Vum_();
                    //Comercial_ objcomer = new Comercial_();
                    //Ingenieria_ objing = new Ingenieria_();

                    //objventanilla.Vum = dtBase.Rows[i]["VUM"].ToString();
                    //objventanilla.Fecha_Recepcion = Convert.ToDateTime(dtBase.Rows[i]["Fecha Recepcion"].ToString());
                    //objventanilla.Fecha_Registro = DateTime.Now;
                    //objventanilla.usuario_registro = Globales.gsMatriculaUsuario;
                    //objventanilla.Cliente_Operador = dtBase.Rows[i]["Cliente Operador"].ToString();
                    //objventanilla.nro_doc_cliente = dtBase.Rows[i]["N° Doc Cliente"].ToString();
                    //objventanilla.fecha_doc_cliente = Convert.ToDateTime(dtBase.Rows[i]["Fecha Doc Cliente"].ToString());
                    //objventanilla.fecha_envio_ing_bajas_postes = Convert.ToDateTime(dtBase.Rows[i]["Fecha envio al Ing. BAJAS APROB. POSTES"].ToString());
                    //objventanilla.contacto_cliente = dtBase.Rows[i]["Contacto Cliente"].ToString();
                    //objventanilla.cliente_final = dtBase.Rows[i]["Cliente Final"].ToString();
                    //objventanilla.tipo_vum = dtBase.Rows[i]["Tipo VUM"].ToString();
                    //objventanilla.requerimiento = dtBase.Rows[i]["Requerimiento"].ToString();
                    //objventanilla.servicio = dtBase.Rows[i]["Servicio"].ToString();
                    //objventanilla.requiere_Estudio = dtBase.Rows[i]["ESTUDIO?"].ToString();
                    //objventanilla.cantidad = Convert.ToInt32(dtBase.Rows[i]["Cantidad"].ToString());
                    //objventanilla.velocidad_inicial = dtBase.Rows[i]["VELOCIDAD INICIAL"].ToString();
                    //objventanilla.unidad_bytes_inicial = dtBase.Rows[i]["UNIDAD BYTES INICIAL"].ToString();
                    //objventanilla.velocidad_implementada = dtBase.Rows[i]["VELOCIDAD IMPLEMENTADA"].ToString();
                    //objventanilla.unidad_bytes_implementada = dtBase.Rows[i]["UNIDAD BYTES IMPLEMENTADA"].ToString();
                    //objventanilla.cd_cr_inicial = dtBase.Rows[i]["CD/CR INICIAL"].ToString();
                    //objventanilla.tipo_interface = dtBase.Rows[i]["TIPO DE INTERFACE"].ToString();
                    //objventanilla.tipo_conector = dtBase.Rows[i]["TIPO DE CONECTOR"].ToString();
                    //objventanilla.router_c_d = dtBase.Rows[i]["CROUTER S/ROUTER"].ToString();

                    //if (dtBase.Rows[i]["FECHA DE PROVISION/BAJA SOLICITADA POR EL CLIENTE"].ToString() != "")                     
                    //{
                    //    objventanilla.fecha_aprovisionamiento_solicitado_cliente = Convert.ToDateTime(dtBase.Rows[i]["FECHA DE PROVISION/BAJA SOLICITADA POR EL CLIENTE"].ToString());
                    //}

                    //objventanilla.nodo_tdp_pop_cliente1 = dtBase.Rows[i]["NODO TDP POP CLIENTE"].ToString();
                    //objventanilla.primeradireccion_popoperador_nodotdp = dtBase.Rows[i]["PRIMERA DIRECCION: POP DEL OPERADOR/NODO TDP"].ToString();
                    //objventanilla.distrito1 = dtBase.Rows[i]["DISTRITO"].ToString();
                    //objventanilla.provincia1 = dtBase.Rows[i]["PROVINCIA"].ToString();
                    //objventanilla.departamento1 = dtBase.Rows[i]["DPTO."].ToString();
                    //objventanilla.nodo_tdp_pop_cliente2 = dtBase.Rows[i]["NODO TDP POP CLIENTE2"].ToString();
                    //objventanilla.segunda_direcccion_clientefinal = dtBase.Rows[i]["SEGUNDA DIRECCION: CLIENTE FINAL"].ToString();
                    //objventanilla.distrito2 = dtBase.Rows[i]["DISTRITO2"].ToString();
                    //objventanilla.provincia2 = dtBase.Rows[i]["PROVINCIA2"].ToString();
                    //objventanilla.departamento2 = dtBase.Rows[i]["DPTO.2"].ToString();
                    //objventanilla.estado_ventanilla = "REVISIÓN PREEVALUACIÓN";
                    //resultado = objvent.Subir_Ventanilla(objventanilla);
                    ////objvent = null;
                    ////b = false;


                    ////SUBIR EL ESTADO A LA  BASE ESTADO_VUM
                    //objestad.Subir_Estado_Vum(objventanilla.Vum, 1);
                    ////SUBIR EL VUM A LA BASE DE COMERCIAL
                    //objcomer.GuardarVum_Comercial(objventanilla.Vum);
                    ////SUBIR EL VUM A LA BASE DE INGENIERA
                    //objing.GuardarVum_Ingenieria(objventanilla.Vum);
                    ////SUBIR EL VUM A LA BASE DE PRECIOS

                    //objventanilla = null;

                }

                MessageBox.Show("Se subio con exito la informacion");
            }
            //x = logBase_Ingreso.Limpiar_Nro_Reclamo();              
            else
            {
                //Dictionary<string, string> parametros = new Dictionary<string, string>();//AUDITORIA
                //parametros = new Dictionary<string, string>();//AUDITORIA
                //parametros.Add("<AccountController>", "Error al escoger el tipo de archivo a guardar: " + Globales.gsMatriculaUsuario);
                //Auditoria.AdministradorLog.escribirLogAuditoria("0", AdministradorLog.ObtenerNombrePorIP(Sis_Tracking_Controversias_UTIL.Globales.gsIPEquipo), parametros);
                //MessageBox.Show("Error al seleccionar el tipo de archivo a subir");
            }

        }

    }
}
