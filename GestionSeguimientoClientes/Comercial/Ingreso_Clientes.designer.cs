﻿namespace GUI_V_2
{
    partial class Ingreso_Ventanilla
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Ingreso_Ventanilla));
            this.GroupBox2 = new System.Windows.Forms.GroupBox();
            this.TabControl1 = new System.Windows.Forms.TabControl();
            this.TabPage1 = new System.Windows.Forms.TabPage();
            this.dgvBase = new System.Windows.Forms.DataGridView();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.btnsubir = new System.Windows.Forms.Button();
            this.BtnImportar = new System.Windows.Forms.Button();
            this.LblRutaArchivo = new System.Windows.Forms.Label();
            this.Label5 = new System.Windows.Forms.Label();
            this.GroupBox2.SuspendLayout();
            this.TabControl1.SuspendLayout();
            this.TabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBase)).BeginInit();
            this.GroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // GroupBox2
            // 
            this.GroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBox2.Controls.Add(this.TabControl1);
            this.GroupBox2.Controls.Add(this.GroupBox1);
            this.GroupBox2.Location = new System.Drawing.Point(12, 12);
            this.GroupBox2.Name = "GroupBox2";
            this.GroupBox2.Size = new System.Drawing.Size(966, 701);
            this.GroupBox2.TabIndex = 40;
            this.GroupBox2.TabStop = false;
            this.GroupBox2.Text = "Subir Bases";
            // 
            // TabControl1
            // 
            this.TabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TabControl1.Controls.Add(this.TabPage1);
            this.TabControl1.Location = new System.Drawing.Point(15, 112);
            this.TabControl1.Name = "TabControl1";
            this.TabControl1.SelectedIndex = 0;
            this.TabControl1.Size = new System.Drawing.Size(945, 583);
            this.TabControl1.TabIndex = 37;
            // 
            // TabPage1
            // 
            this.TabPage1.BackColor = System.Drawing.Color.SteelBlue;
            this.TabPage1.Controls.Add(this.dgvBase);
            this.TabPage1.Location = new System.Drawing.Point(4, 22);
            this.TabPage1.Name = "TabPage1";
            this.TabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage1.Size = new System.Drawing.Size(937, 557);
            this.TabPage1.TabIndex = 0;
            this.TabPage1.Text = "Datos";
            // 
            // dgvBase
            // 
            this.dgvBase.AllowUserToAddRows = false;
            this.dgvBase.AllowUserToDeleteRows = false;
            this.dgvBase.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvBase.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvBase.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvBase.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBase.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvBase.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvBase.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvBase.Location = new System.Drawing.Point(3, 6);
            this.dgvBase.Name = "dgvBase";
            this.dgvBase.Size = new System.Drawing.Size(928, 545);
            this.dgvBase.TabIndex = 1;
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.btnsubir);
            this.GroupBox1.Controls.Add(this.BtnImportar);
            this.GroupBox1.Controls.Add(this.LblRutaArchivo);
            this.GroupBox1.Controls.Add(this.Label5);
            this.GroupBox1.Location = new System.Drawing.Point(15, 19);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(629, 87);
            this.GroupBox1.TabIndex = 35;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Datos";
            // 
            // btnsubir
            // 
            this.btnsubir.BackColor = System.Drawing.Color.SteelBlue;
            this.btnsubir.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnsubir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnsubir.Font = new System.Drawing.Font("Century Gothic", 9F);
            this.btnsubir.ForeColor = System.Drawing.Color.White;
            this.btnsubir.Image = ((System.Drawing.Image)(resources.GetObject("btnsubir.Image")));
            this.btnsubir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsubir.Location = new System.Drawing.Point(523, 30);
            this.btnsubir.Name = "btnsubir";
            this.btnsubir.Size = new System.Drawing.Size(88, 29);
            this.btnsubir.TabIndex = 38;
            this.btnsubir.Text = "Subir";
            this.btnsubir.UseVisualStyleBackColor = false;
            this.btnsubir.Click += new System.EventHandler(this.btnsubir_Click);
            // 
            // BtnImportar
            // 
            this.BtnImportar.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnImportar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnImportar.Font = new System.Drawing.Font("Century Gothic", 9F);
            this.BtnImportar.ForeColor = System.Drawing.Color.White;
            this.BtnImportar.Image = ((System.Drawing.Image)(resources.GetObject("BtnImportar.Image")));
            this.BtnImportar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnImportar.Location = new System.Drawing.Point(413, 30);
            this.BtnImportar.Name = "BtnImportar";
            this.BtnImportar.Size = new System.Drawing.Size(104, 28);
            this.BtnImportar.TabIndex = 38;
            this.BtnImportar.Text = "Importar";
            this.BtnImportar.UseVisualStyleBackColor = false;
            this.BtnImportar.Click += new System.EventHandler(this.BtnImportar_Click);
            // 
            // LblRutaArchivo
            // 
            this.LblRutaArchivo.BackColor = System.Drawing.SystemColors.Window;
            this.LblRutaArchivo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LblRutaArchivo.Cursor = System.Windows.Forms.Cursors.Default;
            this.LblRutaArchivo.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.LblRutaArchivo.Location = new System.Drawing.Point(81, 28);
            this.LblRutaArchivo.Name = "LblRutaArchivo";
            this.LblRutaArchivo.Size = new System.Drawing.Size(304, 42);
            this.LblRutaArchivo.TabIndex = 16;
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(6, 29);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(69, 13);
            this.Label5.TabIndex = 15;
            this.Label5.Text = "Ruta Archivo";
            // 
            // Ingreso_Ventanilla
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(1004, 725);
            this.Controls.Add(this.GroupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Ingreso_Ventanilla";
            this.Text = "Ingreso_Ventanilla";
            this.Load += new System.EventHandler(this.Productos_Load);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Ingreso_Ventanilla_MouseMove);
            this.GroupBox2.ResumeLayout(false);
            this.TabControl1.ResumeLayout(false);
            this.TabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBase)).EndInit();
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        internal System.Windows.Forms.GroupBox GroupBox2;
        internal System.Windows.Forms.TabControl TabControl1;
        internal System.Windows.Forms.TabPage TabPage1;
        internal System.Windows.Forms.DataGridView dgvBase;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.Button btnsubir;
        internal System.Windows.Forms.Button BtnImportar;
        internal System.Windows.Forms.Label LblRutaArchivo;
        internal System.Windows.Forms.Label Label5;
    }
}