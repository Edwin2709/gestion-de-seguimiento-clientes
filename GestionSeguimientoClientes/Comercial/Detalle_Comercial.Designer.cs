﻿namespace Gestion_Oportunidad.Comercial
{
    partial class Detalle_Comercial
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.textBox39 = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.textBox40 = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.textBox41 = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.textBox42 = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.textBox43 = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.textBox44 = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.textBox45 = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.textBox46 = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.textBox47 = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.textBox48 = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.textBox49 = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.textBox50 = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.textBox51 = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.textBox52 = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.textBox53 = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.textBox54 = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.textBox55 = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.textBox56 = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label58 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label74 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.BtnGuardar = new System.Windows.Forms.Button();
            this.textBox57 = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.textBox58 = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.textBox59 = new System.Windows.Forms.TextBox();
            this.textBox60 = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.textBox61 = new System.Windows.Forms.TextBox();
            this.textBox62 = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.textBox63 = new System.Windows.Forms.TextBox();
            this.textBox64 = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox65 = new System.Windows.Forms.TextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.textBox66 = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.textBox67 = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.textBox68 = new System.Windows.Forms.TextBox();
            this.label70 = new System.Windows.Forms.Label();
            this.textBox69 = new System.Windows.Forms.TextBox();
            this.label71 = new System.Windows.Forms.Label();
            this.textBox70 = new System.Windows.Forms.TextBox();
            this.label72 = new System.Windows.Forms.Label();
            this.textBox71 = new System.Windows.Forms.TextBox();
            this.label73 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(986, 704);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Controls.Add(this.groupBox6);
            this.tabPage1.Controls.Add(this.label49);
            this.tabPage1.Controls.Add(this.groupBox9);
            this.tabPage1.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(978, 678);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Ventanilla";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.textBox29);
            this.groupBox5.Controls.Add(this.label29);
            this.groupBox5.Controls.Add(this.textBox34);
            this.groupBox5.Controls.Add(this.label34);
            this.groupBox5.Controls.Add(this.textBox35);
            this.groupBox5.Controls.Add(this.label35);
            this.groupBox5.Controls.Add(this.textBox36);
            this.groupBox5.Controls.Add(this.label36);
            this.groupBox5.Controls.Add(this.textBox37);
            this.groupBox5.Controls.Add(this.label37);
            this.groupBox5.Controls.Add(this.textBox38);
            this.groupBox5.Controls.Add(this.label38);
            this.groupBox5.Controls.Add(this.textBox39);
            this.groupBox5.Controls.Add(this.label39);
            this.groupBox5.Controls.Add(this.textBox40);
            this.groupBox5.Controls.Add(this.label40);
            this.groupBox5.Font = new System.Drawing.Font("Telefonica", 11F);
            this.groupBox5.Location = new System.Drawing.Point(495, 36);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(464, 242);
            this.groupBox5.TabIndex = 22;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Especificaciones";
            // 
            // textBox29
            // 
            this.textBox29.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox29.Location = new System.Drawing.Point(250, 195);
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new System.Drawing.Size(165, 24);
            this.textBox29.TabIndex = 17;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(250, 175);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(128, 16);
            this.label29.TabIndex = 16;
            this.label29.Text = "CROUTER S/ROUTER:";
            // 
            // textBox34
            // 
            this.textBox34.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox34.Location = new System.Drawing.Point(250, 145);
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(165, 24);
            this.textBox34.TabIndex = 15;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(250, 125);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(105, 16);
            this.label34.TabIndex = 14;
            this.label34.Text = "Tipo de Conector:";
            // 
            // textBox35
            // 
            this.textBox35.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox35.Location = new System.Drawing.Point(250, 95);
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new System.Drawing.Size(165, 24);
            this.textBox35.TabIndex = 13;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(250, 75);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(105, 16);
            this.label35.TabIndex = 12;
            this.label35.Text = "Tipo de Interface:";
            // 
            // textBox36
            // 
            this.textBox36.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox36.Location = new System.Drawing.Point(250, 45);
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new System.Drawing.Size(165, 24);
            this.textBox36.TabIndex = 11;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(250, 25);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(82, 16);
            this.label36.TabIndex = 10;
            this.label36.Text = "CD/CR Inicial:";
            // 
            // textBox37
            // 
            this.textBox37.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox37.Location = new System.Drawing.Point(30, 195);
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new System.Drawing.Size(165, 24);
            this.textBox37.TabIndex = 9;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(30, 175);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(61, 16);
            this.label37.TabIndex = 8;
            this.label37.Text = "Cantidad:";
            // 
            // textBox38
            // 
            this.textBox38.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox38.Location = new System.Drawing.Point(30, 145);
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new System.Drawing.Size(165, 24);
            this.textBox38.TabIndex = 7;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(30, 125);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(58, 16);
            this.label38.TabIndex = 6;
            this.label38.Text = "Estudio?:";
            // 
            // textBox39
            // 
            this.textBox39.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox39.Location = new System.Drawing.Point(30, 95);
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new System.Drawing.Size(165, 24);
            this.textBox39.TabIndex = 5;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(30, 75);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(54, 16);
            this.label39.TabIndex = 4;
            this.label39.Text = "Servicio:";
            // 
            // textBox40
            // 
            this.textBox40.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox40.Location = new System.Drawing.Point(30, 45);
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new System.Drawing.Size(165, 24);
            this.textBox40.TabIndex = 3;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(30, 25);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(94, 16);
            this.label40.TabIndex = 2;
            this.label40.Text = "Requerimiento:";
            // 
            // groupBox6
            // 
            this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox6.Controls.Add(this.textBox41);
            this.groupBox6.Controls.Add(this.label41);
            this.groupBox6.Controls.Add(this.textBox42);
            this.groupBox6.Controls.Add(this.label42);
            this.groupBox6.Controls.Add(this.textBox43);
            this.groupBox6.Controls.Add(this.label43);
            this.groupBox6.Controls.Add(this.textBox44);
            this.groupBox6.Controls.Add(this.label44);
            this.groupBox6.Controls.Add(this.textBox45);
            this.groupBox6.Controls.Add(this.label45);
            this.groupBox6.Controls.Add(this.textBox46);
            this.groupBox6.Controls.Add(this.label46);
            this.groupBox6.Controls.Add(this.textBox47);
            this.groupBox6.Controls.Add(this.label47);
            this.groupBox6.Controls.Add(this.textBox48);
            this.groupBox6.Controls.Add(this.label48);
            this.groupBox6.Font = new System.Drawing.Font("Telefonica", 11F);
            this.groupBox6.Location = new System.Drawing.Point(22, 36);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(464, 242);
            this.groupBox6.TabIndex = 20;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Pedido";
            // 
            // textBox41
            // 
            this.textBox41.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox41.Location = new System.Drawing.Point(250, 194);
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new System.Drawing.Size(165, 24);
            this.textBox41.TabIndex = 17;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(250, 175);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(81, 16);
            this.label41.TabIndex = 16;
            this.label41.Text = "Tipo de VUM:";
            // 
            // textBox42
            // 
            this.textBox42.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox42.Location = new System.Drawing.Point(250, 144);
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new System.Drawing.Size(165, 24);
            this.textBox42.TabIndex = 15;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(250, 125);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(79, 16);
            this.label42.TabIndex = 14;
            this.label42.Text = "Cliente Final:";
            // 
            // textBox43
            // 
            this.textBox43.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox43.Location = new System.Drawing.Point(250, 94);
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new System.Drawing.Size(165, 24);
            this.textBox43.TabIndex = 13;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(250, 75);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(104, 16);
            this.label43.TabIndex = 12;
            this.label43.Text = "Contacto Cliente:";
            // 
            // textBox44
            // 
            this.textBox44.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox44.Location = new System.Drawing.Point(250, 45);
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new System.Drawing.Size(165, 24);
            this.textBox44.TabIndex = 11;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(250, 25);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(129, 16);
            this.label44.TabIndex = 10;
            this.label44.Text = "Fecha de Envío al Ing.:";
            // 
            // textBox45
            // 
            this.textBox45.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox45.Location = new System.Drawing.Point(30, 194);
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new System.Drawing.Size(165, 24);
            this.textBox45.TabIndex = 9;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(30, 175);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(112, 16);
            this.label45.TabIndex = 8;
            this.label45.Text = "Fecha Doc. Cliente:";
            // 
            // textBox46
            // 
            this.textBox46.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox46.Location = new System.Drawing.Point(30, 144);
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new System.Drawing.Size(165, 24);
            this.textBox46.TabIndex = 7;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(30, 125);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(95, 16);
            this.label46.TabIndex = 6;
            this.label46.Text = "N° Doc. Cliente:";
            // 
            // textBox47
            // 
            this.textBox47.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox47.Location = new System.Drawing.Point(30, 94);
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new System.Drawing.Size(165, 24);
            this.textBox47.TabIndex = 5;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(30, 75);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(105, 16);
            this.label47.TabIndex = 4;
            this.label47.Text = "Cliente Operador:";
            // 
            // textBox48
            // 
            this.textBox48.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox48.Location = new System.Drawing.Point(30, 45);
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new System.Drawing.Size(165, 24);
            this.textBox48.TabIndex = 3;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(30, 25);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(121, 16);
            this.label48.TabIndex = 2;
            this.label48.Text = "Fecha de Recepción:";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Movistar Text", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label49.Location = new System.Drawing.Point(440, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(104, 27);
            this.label49.TabIndex = 21;
            this.label49.Text = "VUM TEST";
            // 
            // groupBox9
            // 
            this.groupBox9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox9.Controls.Add(this.groupBox7);
            this.groupBox9.Controls.Add(this.groupBox8);
            this.groupBox9.Font = new System.Drawing.Font("Telefonica", 11F);
            this.groupBox9.Location = new System.Drawing.Point(8, 284);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(963, 389);
            this.groupBox9.TabIndex = 23;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Ubicación";
            // 
            // groupBox7
            // 
            this.groupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox7.Controls.Add(this.textBox49);
            this.groupBox7.Controls.Add(this.label50);
            this.groupBox7.Controls.Add(this.textBox50);
            this.groupBox7.Controls.Add(this.label51);
            this.groupBox7.Controls.Add(this.textBox51);
            this.groupBox7.Controls.Add(this.label52);
            this.groupBox7.Controls.Add(this.textBox52);
            this.groupBox7.Controls.Add(this.label53);
            this.groupBox7.Font = new System.Drawing.Font("Telefonica", 10F);
            this.groupBox7.Location = new System.Drawing.Point(487, 25);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(464, 333);
            this.groupBox7.TabIndex = 28;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Destino";
            // 
            // textBox49
            // 
            this.textBox49.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox49.Location = new System.Drawing.Point(72, 217);
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new System.Drawing.Size(273, 24);
            this.textBox49.TabIndex = 27;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(72, 200);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(93, 16);
            this.label50.TabIndex = 26;
            this.label50.Text = "Departamento:";
            // 
            // textBox50
            // 
            this.textBox50.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox50.Location = new System.Drawing.Point(72, 167);
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new System.Drawing.Size(273, 24);
            this.textBox50.TabIndex = 25;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(72, 150);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(61, 16);
            this.label51.TabIndex = 24;
            this.label51.Text = "Provincia:";
            // 
            // textBox51
            // 
            this.textBox51.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox51.Location = new System.Drawing.Point(72, 117);
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new System.Drawing.Size(273, 24);
            this.textBox51.TabIndex = 23;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(72, 100);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(52, 16);
            this.label52.TabIndex = 22;
            this.label52.Text = "Distrito:";
            // 
            // textBox52
            // 
            this.textBox52.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox52.Location = new System.Drawing.Point(72, 67);
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new System.Drawing.Size(273, 24);
            this.textBox52.TabIndex = 21;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(72, 50);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(188, 16);
            this.label53.TabIndex = 20;
            this.label53.Text = "Segunda Dirección: Cliente Final:\r\n";
            // 
            // groupBox8
            // 
            this.groupBox8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox8.Controls.Add(this.textBox53);
            this.groupBox8.Controls.Add(this.label54);
            this.groupBox8.Controls.Add(this.textBox54);
            this.groupBox8.Controls.Add(this.label55);
            this.groupBox8.Controls.Add(this.textBox55);
            this.groupBox8.Controls.Add(this.label56);
            this.groupBox8.Controls.Add(this.textBox56);
            this.groupBox8.Controls.Add(this.label57);
            this.groupBox8.Font = new System.Drawing.Font("Telefonica", 10F);
            this.groupBox8.Location = new System.Drawing.Point(14, 25);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(464, 333);
            this.groupBox8.TabIndex = 0;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Origen";
            // 
            // textBox53
            // 
            this.textBox53.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox53.Location = new System.Drawing.Point(44, 217);
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new System.Drawing.Size(273, 24);
            this.textBox53.TabIndex = 27;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(44, 200);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(93, 16);
            this.label54.TabIndex = 26;
            this.label54.Text = "Departamento:";
            // 
            // textBox54
            // 
            this.textBox54.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox54.Location = new System.Drawing.Point(44, 167);
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new System.Drawing.Size(273, 24);
            this.textBox54.TabIndex = 25;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(44, 150);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(61, 16);
            this.label55.TabIndex = 24;
            this.label55.Text = "Provincia:";
            // 
            // textBox55
            // 
            this.textBox55.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox55.Location = new System.Drawing.Point(44, 117);
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new System.Drawing.Size(273, 24);
            this.textBox55.TabIndex = 23;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(44, 100);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(52, 16);
            this.label56.TabIndex = 22;
            this.label56.Text = "Distrito:";
            // 
            // textBox56
            // 
            this.textBox56.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox56.Location = new System.Drawing.Point(44, 67);
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new System.Drawing.Size(273, 24);
            this.textBox56.TabIndex = 21;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(44, 50);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(273, 16);
            this.label57.TabIndex = 20;
            this.label57.Text = "Primera Dirección: POP del Operador/Nodo TDP:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label58);
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(978, 678);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Ingenieria";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Movistar Text", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label58.Location = new System.Drawing.Point(441, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(104, 27);
            this.label58.TabIndex = 22;
            this.label58.Text = "VUM TEST";
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.textBox30);
            this.groupBox4.Controls.Add(this.label30);
            this.groupBox4.Controls.Add(this.textBox31);
            this.groupBox4.Controls.Add(this.label31);
            this.groupBox4.Controls.Add(this.textBox33);
            this.groupBox4.Controls.Add(this.label33);
            this.groupBox4.Location = new System.Drawing.Point(8, 569);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(785, 92);
            this.groupBox4.TabIndex = 8;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "groupBox4";
            // 
            // textBox30
            // 
            this.textBox30.Location = new System.Drawing.Point(284, 50);
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new System.Drawing.Size(161, 24);
            this.textBox30.TabIndex = 5;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(281, 31);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(87, 16);
            this.label30.TabIndex = 4;
            this.label30.Text = "Fecha Entrega";
            // 
            // textBox31
            // 
            this.textBox31.Location = new System.Drawing.Point(543, 50);
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new System.Drawing.Size(161, 24);
            this.textBox31.TabIndex = 3;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(540, 31);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(164, 16);
            this.label31.TabIndex = 2;
            this.label31.Text = "Fecha Entrega Acta Firmada";
            // 
            // textBox33
            // 
            this.textBox33.Location = new System.Drawing.Point(19, 50);
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new System.Drawing.Size(161, 24);
            this.textBox33.TabIndex = 1;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(16, 31);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(102, 16);
            this.label33.TabIndex = 0;
            this.label33.Text = "Fecha Envio Acta";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.textBox32);
            this.groupBox3.Controls.Add(this.label32);
            this.groupBox3.Controls.Add(this.textBox25);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Controls.Add(this.textBox26);
            this.groupBox3.Controls.Add(this.label26);
            this.groupBox3.Controls.Add(this.textBox27);
            this.groupBox3.Controls.Add(this.label27);
            this.groupBox3.Controls.Add(this.textBox28);
            this.groupBox3.Controls.Add(this.label28);
            this.groupBox3.Controls.Add(this.textBox21);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this.textBox22);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.textBox23);
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Controls.Add(this.textBox24);
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Controls.Add(this.textBox17);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.textBox18);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.textBox19);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.textBox20);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.textBox13);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.textBox14);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.textBox15);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.textBox16);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.textBox12);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.textBox11);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.textBox10);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.textBox9);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Location = new System.Drawing.Point(8, 169);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(951, 394);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "groupBox3";
            // 
            // textBox32
            // 
            this.textBox32.Location = new System.Drawing.Point(19, 361);
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new System.Drawing.Size(161, 24);
            this.textBox32.TabIndex = 49;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(16, 342);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(67, 16);
            this.label32.TabIndex = 48;
            this.label32.Text = "Ura - Nodo";
            // 
            // textBox25
            // 
            this.textBox25.Location = new System.Drawing.Point(782, 300);
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new System.Drawing.Size(161, 24);
            this.textBox25.TabIndex = 47;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(779, 281);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(98, 16);
            this.label25.TabIndex = 46;
            this.label25.Text = "Total S/. Sisego";
            // 
            // textBox26
            // 
            this.textBox26.Location = new System.Drawing.Point(540, 300);
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new System.Drawing.Size(161, 24);
            this.textBox26.TabIndex = 45;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(537, 281);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(167, 16);
            this.label26.TabIndex = 44;
            this.label26.Text = "Tendido FO Canalizado (mts)";
            // 
            // textBox27
            // 
            this.textBox27.Location = new System.Drawing.Point(284, 300);
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new System.Drawing.Size(161, 24);
            this.textBox27.TabIndex = 43;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(281, 281);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(139, 16);
            this.label27.TabIndex = 42;
            this.label27.Text = "Tendido FO Aereo (mts)";
            // 
            // textBox28
            // 
            this.textBox28.Location = new System.Drawing.Point(19, 300);
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new System.Drawing.Size(161, 24);
            this.textBox28.TabIndex = 41;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(16, 281);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(254, 16);
            this.label28.TabIndex = 40;
            this.label28.Text = "Distancia : Obras Civiles (canalizacion-zanja)";
            // 
            // textBox21
            // 
            this.textBox21.Location = new System.Drawing.Point(784, 233);
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(161, 24);
            this.textBox21.TabIndex = 39;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(781, 214);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(149, 16);
            this.label21.TabIndex = 38;
            this.label21.Text = "Instalacion de Accesorios";
            // 
            // textBox22
            // 
            this.textBox22.Location = new System.Drawing.Point(542, 233);
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(161, 24);
            this.textBox22.TabIndex = 37;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(539, 214);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(76, 16);
            this.label22.TabIndex = 36;
            this.label22.Text = "Desmontaje";
            // 
            // textBox23
            // 
            this.textBox23.Location = new System.Drawing.Point(286, 233);
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(161, 24);
            this.textBox23.TabIndex = 35;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(283, 214);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(170, 16);
            this.label23.TabIndex = 34;
            this.label23.Text = "Obras Infraes./INSP.PA/OPEX";
            // 
            // textBox24
            // 
            this.textBox24.Location = new System.Drawing.Point(21, 233);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(161, 24);
            this.textBox24.TabIndex = 33;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(18, 214);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(160, 16);
            this.label24.TabIndex = 32;
            this.label24.Text = "Estudio Impacto Ambiental";
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(782, 169);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(161, 24);
            this.textBox17.TabIndex = 31;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(779, 150);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(132, 16);
            this.label17.TabIndex = 30;
            this.label17.Text = "Protocolos de Pruebas";
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(540, 169);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(161, 24);
            this.textBox18.TabIndex = 29;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(537, 150);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(209, 16);
            this.label18.TabIndex = 28;
            this.label18.Text = "S/ Obras Civiles (canalizacion-zanja)";
            // 
            // textBox19
            // 
            this.textBox19.Location = new System.Drawing.Point(284, 169);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(161, 24);
            this.textBox19.TabIndex = 27;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(281, 150);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(54, 16);
            this.label19.TabIndex = 26;
            this.label19.Text = "Licencia";
            // 
            // textBox20
            // 
            this.textBox20.Location = new System.Drawing.Point(19, 169);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(161, 24);
            this.textBox20.TabIndex = 25;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(16, 150);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(209, 16);
            this.label20.TabIndex = 24;
            this.label20.Text = "Costo Diseño (vista local del cliente)";
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(782, 104);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(161, 24);
            this.textBox13.TabIndex = 23;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(779, 85);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(93, 16);
            this.label13.TabIndex = 22;
            this.label13.Text = "Habilitacion FO";
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(540, 104);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(161, 24);
            this.textBox14.TabIndex = 21;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(537, 85);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(70, 16);
            this.label14.TabIndex = 20;
            this.label14.Text = "Tendido FO";
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(284, 104);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(161, 24);
            this.textBox15.TabIndex = 19;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(281, 85);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(191, 16);
            this.label15.TabIndex = 18;
            this.label15.Text = "Hab. Enlaces INCX- Enlaces DETX";
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(19, 104);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(161, 24);
            this.textBox16.TabIndex = 17;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(16, 85);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(142, 16);
            this.label16.TabIndex = 16;
            this.label16.Text = "Fecha Instalacion o Baja";
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(782, 39);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(161, 24);
            this.textBox12.TabIndex = 15;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(779, 20);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(79, 16);
            this.label12.TabIndex = 14;
            this.label12.Text = "Circuito Final";
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(540, 39);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(161, 24);
            this.textBox11.TabIndex = 13;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(537, 20);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(128, 16);
            this.label11.TabIndex = 12;
            this.label11.Text = "Fecha Fin Ejec. Sisego";
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(284, 39);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(161, 24);
            this.textBox10.TabIndex = 11;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(281, 20);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(78, 16);
            this.label10.TabIndex = 10;
            this.label10.Text = "Plazo Sisego";
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(19, 39);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(161, 24);
            this.textBox9.TabIndex = 9;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 20);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(124, 16);
            this.label9.TabIndex = 8;
            this.label9.Text = "Fecha Inicio Ejec. Ing.";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.textBox5);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.textBox8);
            this.groupBox2.Controls.Add(this.textBox6);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.textBox7);
            this.groupBox2.Location = new System.Drawing.Point(517, 30);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(455, 138);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "groupBox2";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(270, 100);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(161, 24);
            this.textBox5.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(26, 20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(122, 16);
            this.label8.TabIndex = 8;
            this.label8.Text = "Numero VUM en ISIS";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(267, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 16);
            this.label5.TabIndex = 14;
            this.label5.Text = "Fecha Pedido";
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(29, 39);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(161, 24);
            this.textBox8.TabIndex = 9;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(270, 39);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(161, 24);
            this.textBox6.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(26, 81);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(159, 16);
            this.label7.TabIndex = 10;
            this.label7.Text = "Fecha Registro VUM en ISIS";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(267, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 16);
            this.label6.TabIndex = 12;
            this.label6.Text = "Numero Pedido";
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(29, 100);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(161, 24);
            this.textBox7.TabIndex = 11;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.textBox4);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.textBox3);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(6, 30);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(505, 138);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(284, 100);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(161, 24);
            this.textBox4.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(281, 81);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(190, 16);
            this.label4.TabIndex = 6;
            this.label4.Text = "Fecha PPTO enviado al Comercial";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(284, 39);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(161, 24);
            this.textBox3.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(281, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(143, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Fecha PPTO Consolidado";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(19, 100);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(161, 24);
            this.textBox2.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(124, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Fecha Ingreso Sisego";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(19, 39);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(161, 24);
            this.textBox1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Numero Sisego";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label74);
            this.tabPage3.Controls.Add(this.groupBox10);
            this.tabPage3.Controls.Add(this.groupBox11);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(978, 678);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Comercial - Precios";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Movistar Text", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label74.Location = new System.Drawing.Point(429, 0);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(104, 27);
            this.label74.TabIndex = 23;
            this.label74.Text = "VUM TEST";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.BtnGuardar);
            this.groupBox10.Controls.Add(this.textBox57);
            this.groupBox10.Controls.Add(this.label59);
            this.groupBox10.Controls.Add(this.textBox58);
            this.groupBox10.Controls.Add(this.label60);
            this.groupBox10.Controls.Add(this.label61);
            this.groupBox10.Controls.Add(this.textBox59);
            this.groupBox10.Controls.Add(this.textBox60);
            this.groupBox10.Controls.Add(this.label62);
            this.groupBox10.Controls.Add(this.label63);
            this.groupBox10.Controls.Add(this.textBox61);
            this.groupBox10.Controls.Add(this.textBox62);
            this.groupBox10.Controls.Add(this.label64);
            this.groupBox10.Controls.Add(this.label65);
            this.groupBox10.Controls.Add(this.textBox63);
            this.groupBox10.Controls.Add(this.textBox64);
            this.groupBox10.Controls.Add(this.label66);
            this.groupBox10.Font = new System.Drawing.Font("Telefonica", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox10.Location = new System.Drawing.Point(33, 338);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox10.Size = new System.Drawing.Size(913, 279);
            this.groupBox10.TabIndex = 3;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Precios";
            // 
            // BtnGuardar
            // 
            this.BtnGuardar.AutoSize = true;
            this.BtnGuardar.Location = new System.Drawing.Point(811, 239);
            this.BtnGuardar.Name = "BtnGuardar";
            this.BtnGuardar.Size = new System.Drawing.Size(96, 34);
            this.BtnGuardar.TabIndex = 2;
            this.BtnGuardar.Text = "Guardar";
            this.BtnGuardar.UseVisualStyleBackColor = true;
            // 
            // textBox57
            // 
            this.textBox57.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox57.Location = new System.Drawing.Point(544, 191);
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new System.Drawing.Size(245, 24);
            this.textBox57.TabIndex = 39;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(544, 171);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(35, 16);
            this.label59.TabIndex = 38;
            this.label59.Text = "Días:";
            // 
            // textBox58
            // 
            this.textBox58.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox58.Location = new System.Drawing.Point(544, 143);
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new System.Drawing.Size(245, 24);
            this.textBox58.TabIndex = 37;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(69, 27);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(134, 16);
            this.label60.TabIndex = 24;
            this.label60.Text = "Número de Cotización:";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(544, 123);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(150, 16);
            this.label61.TabIndex = 36;
            this.label61.Text = "Fecha de Retorno de OTE:";
            // 
            // textBox59
            // 
            this.textBox59.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox59.Location = new System.Drawing.Point(69, 47);
            this.textBox59.Name = "textBox59";
            this.textBox59.ReadOnly = true;
            this.textBox59.Size = new System.Drawing.Size(245, 24);
            this.textBox59.TabIndex = 25;
            this.textBox59.Text = "test text";
            // 
            // textBox60
            // 
            this.textBox60.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox60.Location = new System.Drawing.Point(544, 95);
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new System.Drawing.Size(245, 24);
            this.textBox60.TabIndex = 35;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(69, 75);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(226, 16);
            this.label62.TabIndex = 26;
            this.label62.Text = "Fecha de Envío de Cotización al Cliente:";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.Location = new System.Drawing.Point(544, 75);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(135, 16);
            this.label63.TabIndex = 34;
            this.label63.Text = "Fecha de Envío de OTE:";
            // 
            // textBox61
            // 
            this.textBox61.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox61.Location = new System.Drawing.Point(69, 95);
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new System.Drawing.Size(245, 24);
            this.textBox61.TabIndex = 27;
            // 
            // textBox62
            // 
            this.textBox62.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox62.Location = new System.Drawing.Point(544, 47);
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new System.Drawing.Size(245, 24);
            this.textBox62.TabIndex = 33;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.Location = new System.Drawing.Point(69, 123);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(245, 16);
            this.label64.TabIndex = 28;
            this.label64.Text = "Fecha de Retorno de Cotización de Cliente:";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(544, 27);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(81, 16);
            this.label65.TabIndex = 32;
            this.label65.Text = "Número OTE:";
            // 
            // textBox63
            // 
            this.textBox63.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox63.Location = new System.Drawing.Point(69, 143);
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new System.Drawing.Size(245, 24);
            this.textBox63.TabIndex = 29;
            // 
            // textBox64
            // 
            this.textBox64.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox64.Location = new System.Drawing.Point(69, 191);
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new System.Drawing.Size(245, 24);
            this.textBox64.TabIndex = 31;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.Location = new System.Drawing.Point(69, 171);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(224, 16);
            this.label66.TabIndex = 30;
            this.label66.Text = "Días Pendientes de Cotización en CMR:";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.button1);
            this.groupBox11.Controls.Add(this.textBox65);
            this.groupBox11.Controls.Add(this.label67);
            this.groupBox11.Controls.Add(this.textBox66);
            this.groupBox11.Controls.Add(this.label68);
            this.groupBox11.Controls.Add(this.textBox67);
            this.groupBox11.Controls.Add(this.label69);
            this.groupBox11.Controls.Add(this.textBox68);
            this.groupBox11.Controls.Add(this.label70);
            this.groupBox11.Controls.Add(this.textBox69);
            this.groupBox11.Controls.Add(this.label71);
            this.groupBox11.Controls.Add(this.textBox70);
            this.groupBox11.Controls.Add(this.label72);
            this.groupBox11.Controls.Add(this.textBox71);
            this.groupBox11.Controls.Add(this.label73);
            this.groupBox11.Font = new System.Drawing.Font("Telefonica", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox11.Location = new System.Drawing.Point(33, 62);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(913, 244);
            this.groupBox11.TabIndex = 2;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Comercial";
            // 
            // button1
            // 
            this.button1.AutoSize = true;
            this.button1.Location = new System.Drawing.Point(811, 204);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(96, 34);
            this.button1.TabIndex = 40;
            this.button1.Text = "Guardar";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // textBox65
            // 
            this.textBox65.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox65.Location = new System.Drawing.Point(544, 141);
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new System.Drawing.Size(245, 24);
            this.textBox65.TabIndex = 23;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(544, 121);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(133, 16);
            this.label67.TabIndex = 22;
            this.label67.Text = "Plazo de Contratación:";
            // 
            // textBox66
            // 
            this.textBox66.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox66.Location = new System.Drawing.Point(544, 93);
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new System.Drawing.Size(245, 24);
            this.textBox66.TabIndex = 21;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.Location = new System.Drawing.Point(544, 73);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(143, 16);
            this.label68.TabIndex = 20;
            this.label68.Text = "Dirección de Instalación:";
            // 
            // textBox67
            // 
            this.textBox67.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox67.Location = new System.Drawing.Point(544, 45);
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new System.Drawing.Size(245, 24);
            this.textBox67.TabIndex = 19;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.Location = new System.Drawing.Point(544, 25);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(65, 16);
            this.label69.TabIndex = 18;
            this.label69.Text = "Velocidad:";
            // 
            // textBox68
            // 
            this.textBox68.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox68.Location = new System.Drawing.Point(72, 194);
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new System.Drawing.Size(245, 24);
            this.textBox68.TabIndex = 17;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.Location = new System.Drawing.Point(72, 174);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(54, 16);
            this.label70.TabIndex = 16;
            this.label70.Text = "Servicio:";
            // 
            // textBox69
            // 
            this.textBox69.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox69.Location = new System.Drawing.Point(72, 146);
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new System.Drawing.Size(245, 24);
            this.textBox69.TabIndex = 15;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.Location = new System.Drawing.Point(72, 126);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(50, 16);
            this.label71.TabIndex = 14;
            this.label71.Text = "Cliente:";
            // 
            // textBox70
            // 
            this.textBox70.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox70.Location = new System.Drawing.Point(72, 98);
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new System.Drawing.Size(245, 24);
            this.textBox70.TabIndex = 13;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.Location = new System.Drawing.Point(69, 78);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(37, 16);
            this.label72.TabIndex = 12;
            this.label72.Text = "VUM:";
            // 
            // textBox71
            // 
            this.textBox71.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox71.Location = new System.Drawing.Point(72, 50);
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new System.Drawing.Size(245, 24);
            this.textBox71.TabIndex = 11;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Telefonica", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.Location = new System.Drawing.Point(69, 25);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(190, 16);
            this.label73.TabIndex = 10;
            this.label73.Text = "Fecha de Inicio Ejecución Cliente:";
            // 
            // Detalle_Comercial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.tabControl1);
            this.Name = "Detalle_Comercial";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Comercial";
            this.Load += new System.EventHandler(this.Comercial_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox textBox39;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox textBox40;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox textBox41;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox textBox42;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox textBox43;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox textBox44;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox textBox45;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox textBox46;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox textBox47;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox textBox48;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox textBox49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox textBox50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox textBox51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox textBox52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox textBox53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox textBox54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox textBox55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox textBox56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Button BtnGuardar;
        private System.Windows.Forms.TextBox textBox57;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox textBox58;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox textBox59;
        private System.Windows.Forms.TextBox textBox60;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.TextBox textBox61;
        private System.Windows.Forms.TextBox textBox62;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox textBox63;
        private System.Windows.Forms.TextBox textBox64;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox65;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.TextBox textBox66;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TextBox textBox67;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.TextBox textBox68;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.TextBox textBox69;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.TextBox textBox70;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.TextBox textBox71;
        private System.Windows.Forms.Label label73;
    }
}