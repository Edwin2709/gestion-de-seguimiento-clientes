﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Data.SqlClient;
using GUI_V_2;
using System.Data.OleDb;
using Gestion_Oportunidad;
using Gestion_Oportunidad.Comercial;

namespace CapaPresentacion
{
    public partial class FormLogin : Form
    {
        public FormLogin()
        {
            InitializeComponent();
        }
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);


        private void txtuser_Enter(object sender, EventArgs e)
        {
            if (txtuser.Text == "Usuario") {
                txtuser.Text = "";
                txtuser.ForeColor = Color.LightGray;
            }
        }

        private void txtuser_Leave(object sender, EventArgs e)
        {
            if(txtuser.Text==""){
                txtuser.Text = "Usuario";
                txtuser.ForeColor = Color.Silver;
            }
        }

        private void txtpass_Enter(object sender, EventArgs e)
        {
            if (txtpass.Text == "Contraseña")
            {
                txtpass.Text = "";
                txtpass.ForeColor = Color.LightGray;
                txtpass.UseSystemPasswordChar = true;
            }
        }

        private void txtpass_Leave(object sender, EventArgs e)
        {
            if(txtpass.Text==""){
                txtpass.Text = "Contraseña";
                txtpass.ForeColor = Color.Silver;
                txtpass.UseSystemPasswordChar = false;
            }
        }

        private void btncerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnminimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Opacity = 1;
            txtuser.Text = Environment.UserName;
        }

        private void btnlogin_Click(object sender, EventArgs e)
        {
            Cls_Registro_Dao objEmpleado = new Cls_Registro_Dao();
            
            SqlDataReader Loguear;
            string usuario = txtuser.Text;
            string contraseña = txtpass.Text;

            if (usuario == txtuser.Text)
            {
                lblErrorUsuario.Visible = false;

                if (contraseña == txtpass.Text)
                {
                    lblErrorPass.Visible = false;
                    Loguear = objEmpleado.iniciarSesion(usuario, contraseña);

                    if (Loguear.Read() == true)
                    {
                        string equipo = Loguear[7].ToString();                       
                        Menu_Comercial ObjFP = new Menu_Comercial(equipo);
                        this.Hide();
                        if (ObjFP.ShowDialog(this) == DialogResult.OK)
                        this.Show();
                        this.Close();
                    }
                    else
                    {
                        lblErrorLogin.Text = "Usuario o contraseña invalidas, intente de nuevo";
                        lblErrorLogin.Visible = true;
                        txtpass.Text = "";
                        txtpass_Leave(null, e);
                        txtuser.Focus();

                        //Dictionary<string, string> parametros = new Dictionary<string, string>();//AUDITORIA
                        //parametros.Add("<AccountController>", "Accion Subir Base " + Globales.gsMatriculaUsuario);
                        //Auditoria.AdministradorLog.escribirLogAuditoria("0", AdministradorLog.ObtenerNombrePorIP(Sis_Tracking_Controversias_UTIL.Globales.gsIPEquipo), parametros);
                    }

                }
                else
                {
                    lblErrorPass.Text = contraseña;
                    lblErrorPass.Visible = true;
                }

            }
            else
            {
                lblErrorUsuario.Text = usuario;
                lblErrorUsuario.Visible = true;
            }  

        }

        private void shapeContainer1_Load(object sender, EventArgs e)
        {

        }

        private void btnlogin_Enter(object sender, EventArgs e)
        {
            btnlogin_Click(sender, e);
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
