﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Gestion_Oportunidad;
using Gestion_Oportunidad.Ventanilla;
using Gestion_Oportunidad.Comercial;
using Gestion_Oportunidad.Ingenieria;

namespace GUI_V_2
{
    public partial class Form1 : Form
    {
        string equipo_usuario = string.Empty;
        public Form1(string equipo)
        {
            equipo_usuario = equipo;
            InitializeComponent();
            this.WindowState = FormWindowState.Normal;
        }

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);

        private void btnslide_Click(object sender, EventArgs e)
        {
            if (MenuVertical.Width == 250)
            {
                MenuVertical.Width = 70;
            }
            else
                MenuVertical.Width = 250;
        }

        private void iconcerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void iconmaximizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            iconrestaurar.Visible = true;
            iconmaximizar.Visible = false;
        }

        private void iconrestaurar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            iconrestaurar.Visible = false;
            iconmaximizar.Visible = true;
        }

        private void iconminimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle,0x112,0xf012,0);
        }

        private void AbrirFormInPanel(object Formhijo)
        {
            if (this.panelContenedor.Controls.Count > 0)
                this.panelContenedor.Controls.RemoveAt(0);
            Form fh = Formhijo as Form;
            fh.TopLevel = false;
            fh.Dock = DockStyle.Fill;
            this.panelContenedor.Controls.Add(fh);
            this.panelContenedor.Tag = fh;
            fh.Show();
        }




        private void btnprod_Click(object sender, EventArgs e)
        {

            if (equipo_usuario == "ventanilla")
            {
                //AbrirFormInPanel(new Menu_Ventanilla());
                Menu_Ventanilla Hijo = new Menu_Ventanilla(equipo_usuario);
                Hijo.StartPosition = FormStartPosition.CenterScreen;
                this.Hide();
                if (Hijo.ShowDialog(this) == DialogResult.OK)
                {
                }
                this.Show();
            }

        }

        private void btnlogoInicio_Click(object sender, EventArgs e)
        {
            AbrirFormInPanel(new InicioResumen());

        }

        private void Form1_Load(object sender, EventArgs e)
        {

            btnlogoInicio_Click(null,e);
        }

        private void panelContenedor_Paint(object sender, PaintEventArgs e)
        {
     
        }

        private void BarraTitulo_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (equipo_usuario == "comercial")
            {
                Menu_Comercial Hijo = new Menu_Comercial(equipo_usuario);
                Hijo.StartPosition = FormStartPosition.CenterScreen;
                this.Hide();
                if (Hijo.ShowDialog(this) == DialogResult.OK)
                {
                }
                this.Show();
            }


        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (equipo_usuario == "ingenieria")
            {
                Menu_Comercial Hijo = new Menu_Comercial(equipo_usuario);
                Hijo.StartPosition = FormStartPosition.CenterScreen;
                this.Hide();
                if (Hijo.ShowDialog(this) == DialogResult.OK)
                {
                }
                this.Show();
            }

        }

        private void MenuVertical_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
