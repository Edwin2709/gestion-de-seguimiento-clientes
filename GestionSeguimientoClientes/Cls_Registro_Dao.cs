﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Windows.Forms;
using Sis_GestionOportunidad_DAO;

namespace Gestion_Oportunidad
{
    public class Cls_Registro_Dao 
    {
        private SqlDataReader leer;

        public SqlDataReader iniciarSesion(string user, string pass)
        {

            SqlConnection cn = new SqlConnection(DatabaseHelper.GetConnectionString());
            
            try
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("SELECT * FROM Usuarios WHERE Usuario = @user And Clave = @pass", cn);
                cmd.Parameters.AddWithValue("@user", user);
                cmd.Parameters.AddWithValue("@pass", pass);
                leer = cmd.ExecuteReader();

                
            }
            catch (Exception e)
            {
                Dictionary<string, string> parametros = new Dictionary<string, string>();//AUDITORIA
                parametros.Add("<ErrorDataAccessLayer>", "Error ObtenerReporteDinamico");
                //Auditoria.AdministradorLog.escribirLogAplicacion("0", AdministradorLog.ObtenerNombrePorIP(Sis_Tracking_Controversias_UTIL.Globales.gsIPEquipo), e, parametros);
            }
            finally
            {
               // cn.Close();
            }
            return leer;

        }

        public DataTable Consultar_Clientes(string equipo, string usuario)
        {

            SqlConnection cn = new SqlConnection(DatabaseHelper.GetConnectionString());
            SqlDataAdapter dap = new SqlDataAdapter();
            //int x = 0;
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("SELECT Id, Razon_Social, Direccion, Departamento, Provincia, Macroregion, Representante_Legal, Ventas FROM Clientes where Usuario = @user ORDER BY Razon_Social", cn);
                cmd.Parameters.AddWithValue("@user", usuario);
                cmd.ExecuteNonQuery();
                dap.SelectCommand = cmd;
                dap.Fill(dt);
            }
            catch (Exception e)
            {
                Dictionary<string, string> parametros = new Dictionary<string, string>();//AUDITORIA
                parametros.Add("<ErrorDataAccessLayer>", "Error ObtenerReporteDinamico");
                //Auditoria.AdministradorLog.escribirLogAplicacion("0", AdministradorLog.ObtenerNombrePorIP(Environment.UserName), e, parametros);
            }
            finally
            {
                cn.Close();
            }
            return dt;
        }

        public DataTable Consultar_Clientes_cod(int id)
        {
            SqlConnection cn = new SqlConnection(DatabaseHelper.GetConnectionString());
            SqlDataAdapter dap = new SqlDataAdapter();
            //int x = 0;
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("SELECT * FROM Clientes where Id = @id ORDER BY Razon_Social", cn);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
                dap.SelectCommand = cmd;
                dap.Fill(dt);
            }
            catch (Exception e)
            {
                Dictionary<string, string> parametros = new Dictionary<string, string>();//AUDITORIA
                parametros.Add("<ErrorDataAccessLayer>", "Error ObtenerReporteDinamico");
                //Auditoria.AdministradorLog.escribirLogAplicacion("0", AdministradorLog.ObtenerNombrePorIP(Environment.UserName), e, parametros);
            }
            finally
            {
                cn.Close();
            }
            return dt;
        }

        public int Insertar_DatosRequerimiento(Cls_DatosRequerimiento objdatos)
        {

            SqlConnection cn = new SqlConnection(DatabaseHelper.GetConnectionString());
            SqlDataAdapter dap = new SqlDataAdapter();

            int ID = 0;
            try
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("sp_Insertar_DatosReuqerimiento", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Requerimiento", objdatos.requerimiento);
                cmd.Parameters.AddWithValue("@Servicio", objdatos.servicio);
                cmd.Parameters.AddWithValue("@Descripcion", objdatos.descripcion);
                cmd.Parameters.AddWithValue("@Cod_Cliente", objdatos.cod_cliente);
                SqlParameter returnParameter = cmd.Parameters.Add("Resultado", SqlDbType.Int);
                returnParameter.Direction = ParameterDirection.ReturnValue;
                cmd.ExecuteNonQuery();
                ID = (int)returnParameter.Value;

            }
            catch (Exception e)
            {
                Dictionary<string, string> parametros = new Dictionary<string, string>();//AUDITORIA
                parametros.Add("<ErrorDataAccessLayer>", "Error ObtenerReporteDinamico");
               //Auditoria.AdministradorLog.escribirLogAplicacion("0", AdministradorLog.ObtenerNombrePorIP(Environment.UserName), e, parametros);
            }
            finally
            {
                cn.Close();
            }
            return ID;
        }

        public int Insertar_DatosBitacora(Cls_DatosBitacora objdatos)
        {
            SqlConnection cn = new SqlConnection(DatabaseHelper.GetConnectionString());
            SqlDataAdapter dap = new SqlDataAdapter();
            int ID = 0;

            try    
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("INSERT INTO Bitacora_Seguimiento(Tipo_Comunicacion,Persona_Contactada,Numero_Llamada, Observaciones, Cod_Requerimiento, Fecha_Registro, Hora_Registro, Origen, Destino) VALUES(@Tipo_Comunicacion,@Persona_Contactada,@Numero_Llamada, @Observaciones, @Cod_Requerimiento, @Fecha_Registro, @Hora_Registro, @Origen, @Destino)", cn);
                cmd.Parameters.AddWithValue("@Tipo_Comunicacion", objdatos.tipocomunicacion);
                cmd.Parameters.AddWithValue("@Persona_Contactada", objdatos.personacontactada);
                cmd.Parameters.AddWithValue("@Numero_Llamada", objdatos.numerollamada);
                cmd.Parameters.AddWithValue("@Observaciones", objdatos.observaciones);
                cmd.Parameters.AddWithValue("@Cod_Requerimiento", objdatos.codrequerimiento);
                cmd.Parameters.AddWithValue("@Fecha_Registro",  Convert.ToDateTime(objdatos.fechaRegistro));
                cmd.Parameters.AddWithValue("@Hora_Registro", Convert.ToDateTime(objdatos.horaRegistro));
                cmd.Parameters.AddWithValue("@Origen", objdatos.origen);
                cmd.Parameters.AddWithValue("@Destino", objdatos.destino);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Dictionary<string, string> parametros = new Dictionary<string, string>();//AUDITORIA
                parametros.Add("<ErrorDataAccessLayer>", "Error ObtenerReporteDinamico");
                //Auditoria.AdministradorLog.escribirLogAplicacion("0", AdministradorLog.ObtenerNombrePorIP(Environment.UserName), e, parametros);
            }
            finally
            {
                cn.Close();
            }
            return ID;
        }


        public string Consultar_Numero_Contacto(string contacto)
        {
            SqlConnection cn = new SqlConnection(DatabaseHelper.GetConnectionString());
            SqlDataAdapter dap = new SqlDataAdapter();
            //int x = 0;
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("SELECT Numero_Llamada FROM Bitacora_Seguimiento WHERE Persona_Contactada = @1", cn);
                cmd.Parameters.AddWithValue("@1", contacto);
                cmd.ExecuteNonQuery();
                dap.SelectCommand = cmd;
                dap.Fill(dt);
            }
            catch (Exception e)
            {
                Dictionary<string, string> parametros = new Dictionary<string, string>();//AUDITORIA
                parametros.Add("<ErrorDataAccessLayer>", "Error ObtenerReporteDinamico");
                //Auditoria.AdministradorLog.escribirLogAplicacion("0", AdministradorLog.ObtenerNombrePorIP(Environment.UserName), e, parametros);
            }
            finally
            {
                cn.Close();
            }
            return dt.Rows[0][0].ToString();
        }


        public DataTable Consultar_Bitacora_x_CodRequerimiento(int codRequerimiento)
        {

            SqlConnection cn = new SqlConnection(DatabaseHelper.GetConnectionString());
            SqlDataAdapter dap = new SqlDataAdapter();
            //int x = 0;
            DataTable dt = new DataTable(); 
            try
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("SELECT b.Tipo_Comunicacion, b.Persona_Contactada, b.Numero_Llamada, b.Fecha_Registro, b.Hora_Registro ,b.Observaciones FROM (Requerimiento r inner join Bitacora_Seguimiento b on r.Id = b.Cod_Requerimiento) where r.Id = @codRequerim ORDER BY b.Fecha_Registro, b.Hora_Registro", cn);
                cmd.Parameters.AddWithValue("@codRequerim", codRequerimiento);
                cmd.ExecuteNonQuery();
                dap.SelectCommand = cmd;
                dap.Fill(dt);
            }
            catch (Exception e)
            {
                Dictionary<string, string> parametros = new Dictionary<string, string>();//AUDITORIA
                parametros.Add("<ErrorDataAccessLayer>", "Error ObtenerReporteDinamico");
                //Auditoria.AdministradorLog.escribirLogAplicacion("0", AdministradorLog.ObtenerNombrePorIP(Environment.UserName), e, parametros);
            }
            finally
            {
                cn.Close();
            }
            return dt;
        }

        public DataTable Consultar_Requerimiento_x_CodCliente(Int32 CodCliente)
        {
            SqlConnection cn = new SqlConnection(DatabaseHelper.GetConnectionString());
            SqlDataAdapter dap = new SqlDataAdapter();
            //int x = 0;
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("SELECT * FROM (Clientes c inner join Requerimiento r on c.Id = r.Cod_Cliente) where c.Id = @id", cn);
                cmd.Parameters.AddWithValue("@id", CodCliente);
                cmd.ExecuteNonQuery();
                dap.SelectCommand = cmd;
                dap.Fill(dt);
            }
            catch (Exception e)
            {
                Dictionary<string, string> parametros = new Dictionary<string, string>();//AUDITORIA
                parametros.Add("<ErrorDataAccessLayer>", "Error ObtenerReporteDinamico");
                //Auditoria.AdministradorLog.escribirLogAplicacion("0", AdministradorLog.ObtenerNombrePorIP(Environment.UserName), e, parametros);
            }
            finally
            {
                cn.Close();
            }
            return dt;
        }
        public AutoCompleteStringCollection GetData()
        {
            SqlConnection cn = new SqlConnection(DatabaseHelper.GetConnectionString());
            SqlDataAdapter dap = new SqlDataAdapter();

            AutoCompleteStringCollection stringColection = new AutoCompleteStringCollection();
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("SELECT DISTINCT RAZON_SOCIAL FROM Clientes", cn);
                cmd.ExecuteNonQuery();
                dap.SelectCommand = cmd;
                dap.Fill(dt);

                foreach (DataRow row in dt.Rows)
                {
                    stringColection.Add(Convert.ToString(row[0]));
                }
        
           
            }
            catch (Exception e)
            {
                Dictionary<string, string> parametros = new Dictionary<string, string>();//AUDITORIA
                parametros.Add("<ErrorDataAccessLayer>", "Error ObtenerReporteDinamico");
                //Auditoria.AdministradorLog.escribirLogAplicacion("0", AdministradorLog.ObtenerNombrePorIP(Environment.UserName), e, parametros);
            }
            finally
            {
                cn.Close();
            }
            return stringColection;

        }

        public AutoCompleteStringCollection GetContactoData()
        {
            SqlConnection cn = new SqlConnection(DatabaseHelper.GetConnectionString());
            SqlDataAdapter dap = new SqlDataAdapter();

            AutoCompleteStringCollection stringColection = new AutoCompleteStringCollection();
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("SELECT DISTINCT Persona_Contactada FROM Bitacora_Seguimiento", cn);
                cmd.ExecuteNonQuery();
                dap.SelectCommand = cmd;
                dap.Fill(dt);

                foreach (DataRow row in dt.Rows)
                {
                    stringColection.Add(Convert.ToString(row[0]));
                }


            }
            catch (Exception e)
            {
                Dictionary<string, string> parametros = new Dictionary<string, string>();//AUDITORIA
                parametros.Add("<ErrorDataAccessLayer>", "Error ObtenerReporteDinamico");
                //Auditoria.AdministradorLog.escribirLogAplicacion("0", AdministradorLog.ObtenerNombrePorIP(Environment.UserName), e, parametros);
            }
            finally
            {
                cn.Close();
            }
            return stringColection;

        }

        public AutoCompleteStringCollection GetLlamadaData()
        {
            SqlConnection cn = new SqlConnection(DatabaseHelper.GetConnectionString());
            SqlDataAdapter dap = new SqlDataAdapter();

            AutoCompleteStringCollection stringColection = new AutoCompleteStringCollection();
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("SELECT DISTINCT Numero_Llamada FROM Bitacora_Seguimiento", cn);
                cmd.ExecuteNonQuery();
                dap.SelectCommand = cmd;
                dap.Fill(dt);

                foreach (DataRow row in dt.Rows)
                {
                    stringColection.Add(Convert.ToString(row[0]));
                }


            }
            catch (Exception e)
            {
                Dictionary<string, string> parametros = new Dictionary<string, string>();//AUDITORIA
                parametros.Add("<ErrorDataAccessLayer>", "Error ObtenerReporteDinamico");
                //Auditoria.AdministradorLog.escribirLogAplicacion("0", AdministradorLog.ObtenerNombrePorIP(Environment.UserName), e, parametros);
            }
            finally
            {
                cn.Close();
            }
            return stringColection;

        }


        public DataTable Consultar_Clientes_Razosocial(string razonsocial)
        {
            SqlConnection cn = new SqlConnection(DatabaseHelper.GetConnectionString());
            SqlDataAdapter dap = new SqlDataAdapter();
            //int x = 0;
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("SELECT * FROM Clientes where RAZON_SOCIAL= @razon_social", cn);
                cmd.Parameters.AddWithValue("@razon_social", razonsocial);
                cmd.ExecuteNonQuery();
                dap.SelectCommand = cmd;
                dap.Fill(dt);
            }
            catch (Exception e)
            {
                Dictionary<string, string> parametros = new Dictionary<string, string>();//AUDITORIA
                parametros.Add("<ErrorDataAccessLayer>", "Error ObtenerReporteDinamico");
                //Auditoria.AdministradorLog.escribirLogAplicacion("0", AdministradorLog.ObtenerNombrePorIP(Environment.UserName), e, parametros);
            }
            finally
            {
                cn.Close();
            }
            return dt;
        }

        public DataTable Consultar_Clientes_Razosocial_(string razonsocial)
        {

            SqlConnection cn = new SqlConnection(DatabaseHelper.GetConnectionString());
            SqlDataAdapter dap = new SqlDataAdapter();
            //int x = 0;
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("SELECT * FROM Clientes  RAZON_SOCIAL LIKE '" + razonsocial + "%'", cn);
                cmd.ExecuteNonQuery();
                dap.SelectCommand = cmd;
                dap.Fill(dt);
            }
            catch (Exception e)
            {
                Dictionary<string, string> parametros = new Dictionary<string, string>();//AUDITORIA
                parametros.Add("<ErrorDataAccessLayer>", "Error ObtenerReporteDinamico");
                //Auditoria.AdministradorLog.escribirLogAplicacion("0", AdministradorLog.ObtenerNombrePorIP(Environment.UserName), e, parametros);
            }
            finally
            {
                cn.Close();
            }
            return dt;
        }

    }
}
